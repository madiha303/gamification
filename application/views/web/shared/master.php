<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Gamification</title>

<link href="<?php echo base_url(); ?>assets/css/administration.css" rel="stylesheet" type="text/css" />

<?php $this->load->view('student/shared/masterscripts');?>

</head>

<body> 

<table width="100%" border="0" cellspacing="0" cellpadding="0">

  <tr>
    <td>    
    	<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
          <tr>
            <td class="header">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                  <tr>
                    <td width="32%" height="100" valign="top"><img src="<?php echo base_url(); ?>assets/images/10_admin_logo.png" alt="" border="0" align="absmiddle" title="" width="397" height="142" /></td>
                  <td width="32%" valign="middle">&nbsp;</td>
                  </tr>
                  
                  <tr>
                    <td height="100" valign="middle">
                    <a href="<?php echo base_url(); ?>/administration/login"> <img src="<?php echo base_url(); ?>/assets/images/admin.png" width="397" height="142" alt="Admin Login" /></a>
                    </td>
                    <td height="100" valign="middle">
                    <a href="<?php echo base_url(); ?>teacher/login"> <img src="<?php echo base_url(); ?>/assets/images/teacher login.png" width="397" height="142" alt="Teacher Login" /></a>
                    </td>
                    <td height="100" valign="middle">
                    <a href="<?php echo base_url(); ?>student/login"> <img src="<?php echo base_url(); ?>/assets/images/student login.png" width="397" height="142" alt="Student Login" /></a>
                    </td>
                  </tr
                  
                </table>
            </td>
          </tr>
        </table>        
    </td>
  </tr>

  <tr>
    <td valign="top"  >
    	<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center"> 
          <tr>
            <td>
                <div id="main">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td height="30">
                        	<?PHP if(isset($error) && $error !=""){echo '<div class="error-box">'.$error.'</div>';}?>
							<?php echo $this->session->flashdata('response');?>
                        </td>
                      </tr>
                      <tr>
                        <td><?php $this->load->view($page_view);?></td>
                      </tr>
                    </table>				
                </div>
            </td>
          </tr> 
        </table>
    </td>
  </tr>

  <tr>
    <td>
    	<table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
         <tr>
           <td>&nbsp;</td>
         </tr>
         <tr>
            <td style="color:#cccccc;">Thesis Demo: Gamified Assignment Submission System &nbsp; Copyright &copy; <?PHP echo date('Y');?> </td><td align="right" style="color:#cccccc;">Demo By: Madiha Javed</td>
          </tr>
           <tr>
           <td>&nbsp;</td>
         </tr>  
        </table>
    </td>
  </tr>

</table>  
</body>
</html>