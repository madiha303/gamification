
<script type="text/javascript" charset="utf-8">
$m(document).ready(function() {
	 initDataTables();
});	
	
function initDataTables(){	
 $m('#example').dataTable( {

		"bJQueryUI": true,	
		"aLengthMenu": [[10, 25 , 50, 100, -1],[10, 25 , 50, 100, "All"]],     
		"sPaginationType": "full_numbers",
		"bProcessing": true,
		"bStateSave": true,			
		"bAutoWidth": false,		
		"sDom": '<"H"CTlfr>t<"F"ilp>',
		
		"oTableTools": 
			{
				"aButtons": 
				[
					/*{						
						
						"sExtends":    "text",
						"sButtonText": "Add New Grade Item",						
						"fnClick": function ( nButton, oConfig, oFlash ) 
						{
							window.location = '<?PHP echo base_url();?>teacher/gradebook/add'
						}
					}*/
					
				]		
				
		},  				
				
		"aoColumns": [
						{ "sWidth": "auto"},  
						{ "sWidth": "auto"},
						{ "sWidth": "auto"},
						{ "sWidth": "auto" },
						{ "sWidth": "auto" },
						{ "sWidth": "auto" },
						                  
					
					 ],					
		
		"fnDrawCallback": function() 
		{},				
		
		"sAjaxSource": "<?PHP echo base_url()?>student/gradebook/get_approved_invitation_table/"
		
	} );	
			
}	
function cnfrm()
{
	return confirm('Are you sure you want to continue?');
}
</script>
    <fieldset>
   <?php //var_dump($receviedid);?>
    <legend><h2><?php echo $page_title; ?></h2></legend>  
    <table id="example" width="100%" cellpadding="0" cellspacing="0" class="dataTableGridNJ">    
    <thead>    
        <tr>
           
            <th align="left"><strong> Title</strong></th>
            <th align="left"><strong>Course</strong></th>
            <th align="left"><strong>Submission Time</strong></th>
            <th align="left"><strong>Acceptance Marks</strong></th>
          <th align="left"><strong>Attendance Marks</strong></th>
            <th align="left"><strong>Last Modified</strong></th>    
          
        </tr>
    </thead>
    <!--<tfoot>
        <tr>    	
            <th align="left">First Name</th>
            <th align="left">Last Name</th>
            <th align="left">Email</th>
            <th align="left">Website</th>
            <th align="left"></th>
        </tr>
    </tfoot>-->
    </table>
    </fieldset>
