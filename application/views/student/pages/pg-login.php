<form name="loginForm" id="loginForm" action="<?php echo base_url(); ?>student/login/authenticate" method="post" >     
<fieldset>
<legend><h2><?php echo $page_title; ?></h2></legend>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td width="20%" height="80%">
            <img src="<?PHP echo base_url();?>assets/images/administration/icons/login_icon.png"/>                     
        </td>
        <td>           
            <p>
                <label for="student_rollno">Roll No:</label>
                
                <input name="student_rollno" type="text" value="" size="35" class="validate[required,minSize[5]]" style="text-transform:none;" />
            </p>
            <p>
                <label for="student_password">Password:</label>
               
                <input name="student_password" type="password" value="" size="35" class="validate[required,minSize[5]]" style="text-transform:none;"/>
            </p>
            
            <p>
                <input type="submit" value="Log On &raquo;" />
            </p>
        </td>
   </tr>
</table>
</fieldset>       
</form>