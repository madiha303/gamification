<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Administration : <?php echo $page_title; ?></title>

<link href="<?php echo base_url(); ?>assets/css/administration.css" rel="stylesheet" type="text/css" />
<?php $this->load->view('administration/shared/masterscripts');?>
</head>

<body> 

<table width="100%" border="0" cellspacing="0" cellpadding="0">

  <tr>
    <td>    
    	<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
          <tr>
            <td class="header">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                  <tr>
                    <td width="25%" height="100" valign="middle"><img src="<?php echo base_url(); ?>assets/images/10_admin_logo.png" alt="" border="0" align="absmiddle" title="" width="250" height="100" /></td>
                    <td width="25%" height="100" valign="middle">&nbsp;</td>
                  </tr>
                  
                  <tr>
                  <td width="100%" valign="bottom">
                   <?PHP if($this->session->userdata('loggedinuser')){?> 
                   
                     <ul id="menu">
                     	
                        <li><a href="<?php echo base_url(); ?>administration/cpanel" class="<?php if ( $this->router->fetch_class() == 'cpanel' ){ echo 'navnew1';}?>">&raquo; Home</a></li>
                        <li><a href="<?php echo base_url(); ?>administration/student"class="<?php if ( $this->router->fetch_class() == 'student' ){ echo 'navnew1';}?>">&raquo; Student</a></li>
                        <li><a href="<?php echo base_url(); ?>administration/teacher" class="<?php if ( $this->router->fetch_class() == 'teacher' ){ echo 'navnew1';}?>"> &raquo; Teacher</a></li>
                        <li><a href="<?php echo base_url(); ?>administration/course" class="<?php if ( $this->router->fetch_class() == 'course' ){ echo 'navnew1';}?>">&raquo; Course</a></li>
                        <li><a href="<?php echo base_url(); ?>administration/announcement" class="<?php if ( $this->router->fetch_class() == 'announcement' ){ echo 'navnew1';}?>">&raquo; Announcement</a></li>
                        <li><a href="<?php echo base_url(); ?>administration/studentcourse/index2" class="<?php if ( $this->router->fetch_class() == 'studentcourse' ||($this->router->fetch_class() == 'teachercourse') ){ echo 'navnew1';}?>">&raquo; Assign Course</a>
                        	<ul>
                                <li><a href="<?php echo base_url(); ?>administration/studentcourse" class="<?php if ( $this->router->fetch_class() == 'studentcourse' ){ echo 'navnew1';}?>">&raquo; To Student</a></li>
                                <li><a href="<?php echo base_url(); ?>administration/teachercourse" class="<?php if ( $this->router->fetch_class() == 'teachercourse' ){ echo 'navnew1';}?>">&raquo; To Teacher</a></li>
                              
                            </ul>
                        </li>
                        <li><a href="<?php echo base_url(); ?>administration/accountsettings" class="<?php if ( $this->router->fetch_class() == 'accountsettings' ){ echo 'navnew1';}?>">&raquo; Account Settings</a></li> 
                        <li><a href="<?php echo base_url(); ?>administration/login/logoff">&raquo; Logout</a></li>                            	
                    </ul> 
                  <?PHP }else{echo "&nbsp;";}?> 
                    </td>
                    
                  </tr>
                  
                </table>
            </td>
          </tr>
        </table>        
    </td>
  </tr>

  <tr>
    <td valign="top"  >
    	<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center"> 
          <tr>
            <td>
                <div id="main">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td height="30">
                        	<?PHP if(isset($error) && $error !=""){echo '<div class="error-box">'.$error.'</div>';}?>
							<?php echo $this->session->flashdata('response');?>
                        </td>
                      </tr>
                      <tr>
                        <td><?php $this->load->view($page_view);?></td>
                      </tr>
                    </table>				
                </div>
            </td>
          </tr> 
        </table>
    </td>
  </tr>

  <tr>
    <td>
    	<table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
         <tr>
           <td>&nbsp;</td>
         </tr>
         <tr>
            <td style="color:#cccccc;">Thesis Demo: Gamified Assignment Submission System &nbsp; Copyright &copy; <?PHP echo date('Y');?> </td><td align="right" style="color:#cccccc;">Demo By: Madiha Javed</td>
          </tr>
           <tr>
           <td>&nbsp;</td>
         </tr>  
        </table>
    </td>
  </tr>

</table>  
</body>
</html>