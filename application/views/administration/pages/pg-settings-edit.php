<form name="settingsFrm" id="settingsFrm" action="<?php echo base_url(); ?>administration/settings/save" method="post" >  
<?PHP 
if($modules)
{

	foreach($modules as $module)
	{
		
		?>
        <fieldset style="margin-bottom:20px;">
        <legend><h2><?php echo $module->module; ?></h2></legend>
        
        <?php
		
		$fields = $this->db_model->get_rows("settings",array("module" => $module->module));
		
		if($fields)
		{
			foreach($fields as $field)
			{
		?>	  
       		
            <p>
                <label><strong><?PHP echo $field->label;?>:</strong></label>
                <?PHP 				
				if($field->field_type == 'text')
				{
				?>
                <input type="text" name="<?PHP echo $field->key;?>" id="<?PHP echo $field->key;?>" value="<?PHP echo $field->value;?>" size="60"/>
                <?PHP 
				}
				else if($field->field_type == 'textarea')
				{
				?>
                <textarea name="<?PHP echo $field->key;?>" id="<?PHP echo $field->key;?>" style="width:1140px;" rows="12"><?PHP echo $field->value;?></textarea>
                <?PHP 
				}
				else if($field->field_type == 'select')
				{
				?>
                <select name="<?PHP echo $field->key;?>" id="<?PHP echo $field->key;?>">
                	<option value="Activate" <?PHP if($field->value == 'Activate'){echo "Selected";}?>>Activate</option>
                    <option value="Deactivate" <?PHP if($field->value == 'Deactivate'){echo "Selected";}?>>Deactivate</option>
                </select>
                <?PHP 
				}
				?>
           </p>
              
	<?PHP
			}
		}
		
		?>     

        </fieldset>
        
        <?PHP 
		
		
	}
}
?>
<p>
    <input type="submit" value="Save &raquo;" />
    <input type="button" onclick="window.location='<?PHP echo base_url();?>administration/vehicles'" value="cancel"/>               
</p>
</form>