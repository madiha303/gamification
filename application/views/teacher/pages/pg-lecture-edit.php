<script>

$(function(){
	//$('#begining_date').datetimepicker({timeFormat: "hh:mm tt"});
	var startDateTextBox = $('#begining_date');
	var endDateTextBox = $('#ending_date');

	startDateTextBox.datetimepicker({ 
		onClose: function(dateText, inst) {
			if (endDateTextBox.val() != '') {
				var testStartDate = startDateTextBox.datetimepicker('getDate');
				var testEndDate = endDateTextBox.datetimepicker('getDate');
				if (testStartDate > testEndDate)
					endDateTextBox.datetimepicker('setDate', testStartDate);
			}
			else {
				endDateTextBox.val(dateText);
			}
		},
		onSelect: function (selectedDateTime){
			endDateTextBox.datetimepicker('option', 'minDate', startDateTextBox.datetimepicker('getDate') );
		}
	});
	endDateTextBox.datetimepicker({ 
		onClose: function(dateText, inst) {
			if (startDateTextBox.val() != '') {
				var testStartDate = startDateTextBox.datetimepicker('getDate');
				var testEndDate = endDateTextBox.datetimepicker('getDate');
				if (testStartDate > testEndDate)
					startDateTextBox.datetimepicker('setDate', testEndDate);
			}
			else {
				startDateTextBox.val(dateText);
			}
		},
		onSelect: function (selectedDateTime){
			startDateTextBox.datetimepicker('option', 'maxDate', endDateTextBox.datetimepicker('getDate') );
		}
	});

	
});



function countChars() {
     var l = "1000";
     var str = document.getElementById("detail").value;
     var len = str.length;
     if(len <= l) {
          document.getElementById("txtLen").value=l-len;
     } else {
          document.getElementById("detail").value=str.substr(0, 1000);
     }
}

			
</script>
<?php

if(isset($row))
{
	
	//var_dump($row);
	foreach($row as $rows){
	$value_id=$rows->id;
	$value_title=$rows->lecture_title;
	$value_course=$rows->course_title.'   ('.$rows->section.')'; 
	$value_attachment=$rows->attachment;
	
	}
	
	
}

?>

<form id="announementFrm" name="announementFrm"  enctype="multipart/form-data" method="post" action="<?PHP echo base_url();?>teacher/lecture/save">
<fieldset>
<legend><h2><?php echo $page_title; ?></h2></legend>
<table  cellpadding="0" cellspacing="0">
	<tr>
    	<td><?php echo validation_errors('<div style="color:red;">','</div>'); ?></td>
    </tr>
	<tr>
    	<td>
        	<p>
                <label><strong>Title:</strong></label>                    
                <input size="60" name="lecture_title" id="lecture_title" type="text" value="<?PHP if(isset($row)){echo set_value('lecture_title',$value_title);}?>" class="validate[required]"  />
        	</p>
            
            <p>
        	  <label><strong>Course:</strong></label>
        	  <select name="course_id" id="course_id" style="width:382px;"  class="dropdown validate[required]">      
						 <option value="" <?php echo set_select('course_id', '', TRUE); ?>>Select Course</option>
						<?php foreach($row_course as $rows): 
						 $key1=$rows->course_id;
						 
						 	
                         $value=$rows->course_title.'   ('.$rows->section.')'; 
						
						 ?>                                              
                       
                        <option value="<?php echo $key1?>" 
                              <?php  
							   if(isset($row))
								{
								  	if ($value==$value_course)
									{
										echo 'selected';
									}; 
								}
								?>
                          >
                                <?php  echo $value ?>
                        </option>
                       
                        <?php endforeach; ?>
                        
            	</select>                       
       	  </p>
          
           <p>              	
                <label><strong>Attachement: (<?PHP echo $this->config->item('files_types');?>):</strong></label>
                <input size="48" name="attachment" id="attachment" type="file" value="<?PHP if(isset($row)){echo set_value('attachment',$value_attachment);}?>" class="
				<?PHP if(!isset($row))
				{ echo 'validate[required]';
				}?>
                "/>
                
               <?PHP if(isset($row)){
			   if($value_attachment !=""){
				   $file_path= base_url()."assets/lectures/".$value_attachment;
				   ?>
              <p>
                <?php echo $value_attachment;?>&nbsp;<a href="<?PHP echo $file_path;?>" target="_blank"> Download </a>
              </p>
                    <?PHP }}?>
                  </p>
          
             

             
           
             
             <p>
             	<input type="submit" name="btnSubmit" id="btnSubmit" value="Save" /> 
                <input type="button" onclick="window.location='<?PHP echo base_url();?>teacher/lecture'" value="cancel"/>               
             </p>
        </td>
    </tr>
</table>
<input type="hidden" name="mode" id="mode" value="<?PHP echo $mode;?>"/>
<input type="hidden" name="id" id="id" value="<?PHP if(isset($row)){echo $value_id;}?>"/>
<input type="hidden" name="last_modified" id="last_modified" value=""/>

</fieldset>
</form>

