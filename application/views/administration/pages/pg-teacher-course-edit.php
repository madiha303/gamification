
<?php
/*$options = array('Computer Science'=>'Computer Science','Software Engineering'=>'Software Engineering', 'Computer Arts'=>'Computer Arts');
var_dump($row_teacher);
echo "<br/><br/><br/>";
var_dump($row_course);
echo "<br/><br/>";
var_dump($row);
echo "<br/><br/>";*/
if(isset($row))
{
	foreach($row as $val){
	$id=$val->teacher_course_id;
	$value_teacher=$val->teacher_fname.' '.$val->teacher_lname.'--->'.$val->teacher_department;
	$value_course=$val->course_title.'   ('.$val->section.')'; 
	}
}

//var_dump($value_teacher);
//var_dump($value_course);
?>
<form id="teacherFrm" name="teacherFrm"  enctype="multipart/form-data" method="post" action="<?PHP echo base_url();?>administration/teachercourse/save">
<fieldset>
<legend><h2><?php echo $page_title; ?></h2></legend>
<table  cellpadding="0" cellspacing="0">
	<tr>
    	<td><?php echo validation_errors('<div style="color:red;">','</div>'); ?></td>
    </tr>
	<tr>
    	<td>
        	<p>
                <label><strong>Teacher Name:</strong></label>                    
                <select name="teacher_id" id="teacher_id" style="width:382px;" class="dropdown validate[required]" >     
                 <option value="" <?php echo set_select('teacher_id', '', TRUE); ?>>Select Teacher</option> 
						<?php foreach($row_teacher as $rows): 
						 $key1=$rows->teacher_id;	
                         $value=$rows->teacher_fname.' '.$rows->teacher_lname.'--->'.$rows->teacher_department; 
						 
						
						 ?>                                              
                        <option value="<?php echo $key1?>" 
                              <?php   
							  	if(isset($row))
								{
									if ($value == $value_teacher)
									{
										echo 'selected';
									}; 
								}
							  ?>
                          >
                                <?php  echo $value ?>
                        </option>
                       
                        <?php endforeach; ?>
            	</select>
        	</p>
        	<p>
        	  <label><strong>Course:</strong></label>
        	  <select name="course_id" id="course_id" style="width:382px;"  class="dropdown validate[required]">      
						 <option value="" <?php echo set_select('course_id', '', TRUE); ?>>Select Course</option>
						<?php foreach($row_course as $rows): 
						 $key1=$rows->course_id;
						 
						 	
                         $value=$rows->course_title.'   ('.$rows->section.')'; 
						
						 ?>                                              
                       
                        <option value="<?php echo $key1?>" 
                              <?php  
							   if(isset($row))
								{
								  	if ($value==$value_course)
									{
										echo 'selected';
									}; 
								}
								?>
                          >
                                <?php  echo $value ?>
                        </option>
                       
                        <?php endforeach; ?>
                        
            	</select>                       
       	  </p>
          
          
          
          
      
				
            
             	<input type="submit" name="btnSubmit" id="btnSubmit" value="Save" /> 
                <input type="button" onclick="window.location='<?PHP echo base_url();?>administration/teachercourse'" value="cancel"/>               
             </p>
        </td>
    </tr>
</table>

<input type="hidden" name="mode" id="mode" value="<?PHP echo $mode;?>"/>
<input type="hidden" name="id" id="id" value="<?PHP if(isset($row)){echo $id;}?>"/>


</fieldset>
</form>

