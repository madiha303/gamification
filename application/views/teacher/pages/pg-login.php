<form name="loginForm" id="loginForm" action="<?php echo base_url(); ?>teacher/login/authenticate" method="post" >     
<fieldset>
<legend><h2><?php echo $page_title; ?></h2></legend>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td width="20%" height="80%">
            <img src="<?PHP echo base_url();?>assets/images/administration/icons/login_icon.png"/>                     
        </td>
        <td>           
            <p>
                <label for="teacher_login">Username:</label>
                
                <input name="teacher_login" type="text" value="" size="35" class="validate[required,minSize[5]]" style="text-transform:none;" />
            </p>
            <p>
                <label for="teacher_password">Password:</label>
               
                <input name="teacher_password" type="password" value="" size="35" class="validate[required,minSize[5]]" style="text-transform:none;"/>
            </p>
            
            <p>
                <input type="submit" value="Log On &raquo;" />
            </p>
        </td>
   </tr>
</table>
</fieldset>       
</form>