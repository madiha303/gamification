
<?php
//var_dump($row);
$options = array('Computer Science'=>'Computer Science','Software Engineering'=>'Software Engineering', 'Computer Arts'=>'Computer Arts');
?>
<form id="teacherFrm" name="teacherFrm"  enctype="multipart/form-data" method="post" action="<?PHP echo base_url();?>administration/teacher/save">
<fieldset>
<legend><h2><?php echo $page_title; ?></h2></legend>
<table  cellpadding="0" cellspacing="0">
	<tr>
    	<td><?php echo validation_errors('<div style="color:red;">','</div>'); ?></td>
    </tr>
	<tr>
    	<td>
        	<p>
                <label><strong>First Name:</strong></label>                    
                <input size="60" name="teacher_fname" id="teacher_fname" type="text" value="<?PHP echo set_value('teacher_fname',$row->teacher_fname);?>" class="validate[required]" />
        	</p>
        	<p>
        	  <label><strong>Last Name:</strong></label>
        	  <input size="60" name="teacher_lname" id="teacher_lname" type="text" value="<?PHP echo set_value('teacher_lname',$row->teacher_lname);?>" class="validate[required]" />
      	  </p>  
             
            <p>
        	  <label><strong>Department:</strong></label>
              <select name="teacher_department" id="teacher_department" style="width:382px;" >      
            	<?php foreach($options as $value=>$text):?>                                     
                <option value="<?php echo $value ?>" 
                        <?php if ($row->teacher_department == $value)
						{
							echo 'selected';
							}; ?> >
                        <?php echo $text ?>
                </option>
            	<?php endforeach; ?>
            </select>
      	  </p> 
            <p>
        	  <label><strong>Email:</strong></label>
        	  <input size="60" name="teacher_email" id="teacher_email" type="text" value="<?PHP echo set_value('teacher_email',$row->teacher_email);?>" class="validate[required],custom[email]" style="text-transform:none;"  />
      	  </p>  
            
             <p>
               <label><strong>Login:</strong></label>                    
                <input size="60" name="teacher_login" id="teacher_login" type="text" value="<?PHP echo set_value('teacher_login',$row->teacher_login);?>" class="validate[required]" style="text-transform:none;" />
          </p>
             <p>
              <label><strong>Password:</strong></label>                    
                <input size="60" name="teacher_password" id="teacher_password" type="text" value="<?PHP echo set_value('teacher_password',$row->teacher_password);?>" class="validate[required,minSize[5]]" style="text-transform:none;" />
             </p>
             
              <p>              	
                <label><strong>Photo (<?PHP echo $this->config->item('image_types');?>):</strong></label>
                <input size="48" name="photo" id="photo" type="file"/>
                <?PHP if($row->teacher_photo !=""){?>
          <p>
                <img src="<?PHP echo base_url();?>administration/teacher/get_image/<?PHP echo $row->teacher_photo;?>/313/217"/>
          </p>
				<?PHP }?>
              </p>
             

             
           
             
             <p>
             	<input type="submit" name="btnSubmit" id="btnSubmit" value="Save" /> 
                <input type="button" onclick="window.location='<?PHP echo base_url();?>administration/teacher'" value="cancel"/>               
             </p>
        </td>
    </tr>
</table>
<input type="hidden" name="mode" id="mode" value="<?PHP echo $mode;?>"/>
<input type="hidden" name="id" id="id" value="<?PHP if(isset($row)){echo $row->teacher_id;}?>"/>
</fieldset>
</form>

