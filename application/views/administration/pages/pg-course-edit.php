<?php $options = array('A'=>'A','B'=>'B','--'=>'--'); ?>
<form id="courseFrm" name="courseFrm"  enctype="multipart/form-data" method="post" action="<?PHP echo base_url();?>administration/course/save">
<fieldset>
<legend><h2><?php echo $page_title; ?></h2></legend>
<table  cellpadding="0" cellspacing="0">
	<tr>
    	<td><?php echo validation_errors('<div style="color:red;">','</div>'); ?></td>
    </tr>
	<tr>
    	<td>
        	<p>
                <label><strong>Course Title:</strong></label>                    
                <input size="60" name="course_title" id="course_title" type="text" value="<?PHP echo set_value('course_title',$row->course_title);?>" class="validate[required]" />
        	</p>
        	<p>
        	  <label><strong>Course Code:</strong></label>
        	  <input size="60" name="course_code" id="course_code" type="text" value="<?PHP echo set_value('course_code',$row->course_code);?>" class="validate[required]" />
      	  </p>  
             
            <p>
        	  <label><strong>Credit Hour:</strong></label>
        	  <input size="60" name="course_credit" id="course_credit" type="text" class="validate[required]" value="<?PHP echo set_value('course_credit',$row->course_credit);?>"  />
      	  </p>
          
          <p>
        	  <label><strong>Section:</strong></label>
              <select name="section" id="section" style="width:382px;" class="dropdown validate[required]"  >  
               <option value="" <?php echo set_select('section', '', TRUE); ?>>Select Section</option>    
            	<?php foreach($options as $value=>$text):?>                                     
                <option value="<?php echo $value ?>" 
                        <?php 
							if(isset($row->section))
						{
								
						if ($row->section == $value)
						{
							echo 'selected';
							}
						}; ?> >
                        <?php echo $text ?>
                </option>
            	<?php endforeach; ?>
            </select>
      	  </p>
          
            
             <p>
             	<input type="submit" name="btnSubmit" id="btnSubmit" value="Save" /> 
                <input type="button" onclick="window.location='<?PHP echo base_url();?>administration/course'" value="cancel"/>               
             </p>
        </td>
    </tr>
</table>
<input type="hidden" name="mode" id="mode" value="<?PHP echo $mode;?>"/>
<input type="hidden" name="id" id="id" value="<?PHP if(isset($row)){echo $row->course_id;}?>"/>
<input type="hidden" name="assignment_weight" id="assignment_weight" value="15"/>
<input type="hidden" name="invitation_weight" id="invitation_weight" value="5"/>
<input type="hidden" name="question_weight" id="question_weight" value="5"/>
</fieldset>
</form>

