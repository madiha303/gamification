<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Student : <?php echo $page_title; ?></title>

<link href="<?php echo base_url(); ?>assets/css/administration.css" rel="stylesheet" type="text/css" />

<?php $this->load->view('student/shared/masterscripts');?>

</head>

<body> 

<table width="100%" border="0" cellspacing="0" cellpadding="0">

  <tr>
    <td>    
    	<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
          <tr>
            <td class="header">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                  <tr>
                    <td width="25%" height="100" valign="top"><img src="<?php echo base_url(); ?>assets/images/10_admin_logo.png" alt="" border="0" align="absmiddle" title="" width="250" height="100" /></td>
                    <td width="75%" valign="middle">
                   <?PHP if($this->session->userdata('loggedinstudent')){
					   //echo $this->uri->uri_string();?> 
                   
                     <ul id="menu">
                     	
                        <li ><a href="<?php echo base_url(); ?>student/cpanel">&raquo; Home</a></li>
                        <li ><a href="<?php echo base_url(); ?>student/course"  class="<?php if ( $this->router->fetch_class() == 'course' ){ echo 'navnew1';}?>">&raquo; Course Progress Board</a></li>
                       	
                        <li><a href="<?php echo base_url(); ?>student/manage"  class="<?php 
						 if ( ($this->router->fetch_class() == 'manage') ||($this->router->fetch_class() == 'lecture')||($this->router->fetch_class() == 'announcement')||($this->router->fetch_class() == 'assignment')||($this->router->fetch_class() == 'invitation')||$this->router->fetch_class() == 'questionpool'){ echo 'navnew1';}?>">&raquo; Manage</a>
                        	<ul>
                                <li ><a href="<?php echo base_url(); ?>student/announcement"  class="<?php if ( $this->router->fetch_class() == 'announcement' ){ echo 'navnew1';}?>">&raquo; Announcement</a></li>
                                 <li ><a href="<?php echo base_url(); ?>student/assignment"  class="<?php if ( $this->router->fetch_class() == 'assignment' ){ echo 'navnew1';}?>" >&raquo; Assignment</a></li>
                                <li ><a href="<?php echo base_url(); ?>student/invitation"  class="<?php if ( $this->router->fetch_class() == 'invitation' ){ echo 'navnew1';}?>" >&raquo; Invitation</a></li>
                                <li ><a href="<?php echo base_url(); ?>student/lecture"  class="<?php if ( $this->router->fetch_class() == 'lecture' ){ echo 'navnew1';}?>" >&raquo; Lecture</a></li>                           
                                <li ><a href="<?php echo base_url(); ?>student/questionpool"  class="<?php if ( $this->router->fetch_class() == 'questionpool' ){ echo 'navnew1';}?>" >&raquo; Question Pool</a></li>
                              
                            </ul>
                        </li>
                        
                        
                        
                       <!-- <li ><a href="<?php echo base_url(); ?>student/announcement">&raquo; Announcement</a></li>
                        <li ><a href="<?php echo base_url(); ?>student/lecture">&raquo; Lecture</a></li>
                        <li ><a href="<?php echo base_url(); ?>student/assignment">&raquo; Assignment</a></li>
                         <li ><a href="<?php echo base_url(); ?>student/invitation">&raquo; Invitation</a></li>
                        <li ><a href="<?php echo base_url(); ?>student/questionpool">&raquo; Question Pool</a></li>-->
                        
                        <!--------------------------------------------------------------------------------------------------->
                         <!--------------------------------------------------------------------------------------------------->
                        
                        <li><a href="<?php echo base_url(); ?>student/gradebook"  class="<?php 
						 if ( ($this->uri->uri_string() == 'student/gradebook') ||($this->uri->uri_string() == 'student/gradebook/index2')||($this->uri->uri_string() == 'student/gradebook/index3')||($this->uri->uri_string() == 'student/gradebook/index4')){ echo 'navnew1';}?>">&raquo; Marks Book</a>
                        	<ul>
                               <li ><a href="<?php echo base_url(); ?>student/gradebook/index4" class="<?php if ( $this->uri->uri_string() == 'student/gradebook/index4' ){ echo 'navnew1';}?>" >&raquo;Assignment Marks</a></li> 						 
                        		<li ><a href="<?php echo base_url(); ?>student/gradebook/index3" class="<?php if ($this->uri->uri_string() == 'student/gradebook/index3' ){ echo 'navnew1';}?>" >&raquo;Invitation Marks</a></li> 
                         	<li ><a href="<?php echo base_url(); ?>student/gradebook/index2" class="<?php if ( $this->uri->uri_string() == 'student/gradebook/index2' ){ echo 'navnew1';}?>" >&raquo;QuestionPool Marks</a></li> 
                              
                            </ul>
                        </li>
                        
                        
                        
                   		<!--<li ><a href="<?php echo base_url(); ?>student/gradebook">&raquo; Grade Book</a></li> 
                        <li ><a href="<?php echo base_url(); ?>student/gradebook/index4">&raquo; Marked Assignments</a></li>
                         <li ><a href="<?php echo base_url(); ?>student/gradebook/index3">&raquo; Marked Invitations</a></li> 
                       <li ><a href="<?php echo base_url(); ?>student/gradebook/index2">&raquo; Marked Question</a></li>-->
                      
                        <li ><a href="<?php echo base_url(); ?>student/accountsettings" class="<?php if ( $this->router->fetch_class() == 'accountsettings' ){ echo 'navnew1';}?>">&raquo; Account Settings</a></li> 
                         <li><a href="<?php echo base_url(); ?>student/login/logoff">&raquo; Logout</a></li>                            	
                    </ul> 
                  <?PHP }else{echo "&nbsp;";}?> 
                    </td>
                  </tr>
                  
                  <!--<tr>
                    <td height="100" valign="middle">&nbsp;</td>
                    <td valign="middle">&nbsp;</td>
                  </tr>-->
                  
                </table>
            </td>
          </tr>
        </table>        
    </td>
  </tr>

  <tr>
    <td valign="top"  >
    	<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center"> 
          <tr>
            <td>
                <div id="main">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td height="30">
                        	<?PHP if(isset($error) && $error !=""){echo '<div class="error-box">'.$error.'</div>';}?>
							<?php echo $this->session->flashdata('response');?>
                        </td>
                      </tr>
                      <tr>
                        <td><?php $this->load->view($page_view);?></td>
                      </tr>
                    </table>				
                </div>
            </td>
          </tr> 
        </table>
    </td>
  </tr>

  <tr>
    <td>
    	<table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
         <tr>
           <td>&nbsp;</td>
         </tr>
         <tr>
            <td style="color:#cccccc;">Thesis Demo: Gamified Assignment Submission System &nbsp; Copyright &copy; <?PHP echo date('Y');?> </td><td align="right" style="color:#cccccc;">Demo By: Madiha Javed</td>
          </tr>
           <tr>
           <td>&nbsp;</td>
         </tr>  
        </table>
    </td>
  </tr>

</table>  
</body>
</html>