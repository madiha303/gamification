<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class AccountSettings extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 
	public function __construct()
    {
		parent::__construct();
			
		if(!$this->session->userdata('loggedinstudent'))
		{
			$this->session->set_flashdata('response', '<div class="error-box">Please login...!</div>');
			redirect(base_url().'student/login', 'refresh');		
			exit;
		}	
		
		// Your own constructor code    	
	}	
	 
	public function index()
	{
		$data = array(
				'page_title' => "Student Account Settings",
				'page_view' => "student/pages/pg-account-settings"				
				);
														
		$this->load->view('student/shared/master',$data);
	}
	
	public function save()
	{
		
		$vals = $this->input->post();		
		unset($vals['btnSubmit']);	
		$vals['last_modified'] = date('Y-m-d h:i:s');
			
		$where = array('student_id' => $this->session->userdata('loggedinstudent')->student_id);
		
		$res = $this->db_model->update_row("student",$vals,$where);
		
		if($res)
		{
			$this->refreshSession();
			$this->session->set_flashdata('response', '<div class="success-box">Account information has been updated.</div>');
			redirect($_SERVER['HTTP_REFERER'], 'refresh');
		}
		else
		{
			$this->session->set_flashdata('response', '<div class="error-box">Request can not be processed at the moment, please try again later.</div>');
			redirect($_SERVER['HTTP_REFERER'], 'refresh');
		}
	}
	
	function refreshSession()
	{			
		$result = $this->db_model->get_row('student',array('student_id' => $this->session->userdata('loggedinstudent')->student_id));
		$this->session->set_userdata('loggedinstudent', $result);		
	}
	
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */