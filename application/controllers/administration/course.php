<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Course extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 
	private $error = "";
	 
    public function __construct()
    {
		parent::__construct();
		
		if(!$this->session->userdata('loggedinuser'))
		{
			$this->session->set_flashdata('response', '<div class="error-box">Please login...!</div>');
			redirect(base_url().'administration/login', 'refresh');		
			exit;
		}	
		
		// Your own constructor code    	
	}	
	 
	public function index()
	{
          
            $data = array(
				'page_title' => "Course Management",
				'page_view' => "administration/pages/pg-course-view"
				);
														
		$this->load->view('administration/shared/master',$data);
	}
//************** Get Table *****		
	public function get_table()
	{
		$res = $this->db_model->get_table('course');
        echo "{ \"aaData\": [";
		if ($res)
        {
			 $indx = 1;
			 foreach ($res as $row)
             {
				 
				 $indx_id = $row->course_id;
				 
				 $edit_url = "<a href='".base_url()."administration/course/edit/".$indx_id."'><img src='".base_url()."assets/images/administration/icons/edit.gif'/></a>";
				 
				 $del_url = "<a href='".base_url()."administration/course/del/".$indx_id."' onclick='return cnfrm()'><img src='".base_url()."assets/images/administration/icons/del.gif'/></a>";	
					 
				 
				 $options = $edit_url." | ".$del_url;
	
				 
				 if ($indx != sizeof($res))
                 {
					  echo '["'.$row->course_title.'","'.$row->section.'","'.$row->course_code.'","'.$row->course_credit.'","'.date("F j, Y, g:i a",strtotime($row->last_modified)).'","'.$options.'"],';
				 }
				 else
				{
					 echo '["'.$row->course_title.'","'.$row->section.'","'.$row->course_code.'","'.$row->course_credit.'","'.date("F j, Y, g:i a",strtotime($row->last_modified)).'","'.$options.'"]';
				 }
				 
				 $indx++;
			 }
		}
		
		echo "] }";
			
	}	
//************** edit *****	
	
	public function edit($id)
	{
		$data = array(
				'error' => $this->error,
				'page_title' => "Course Management",
				'page_view' => "administration/pages/pg-course-edit",
				'mode' => "edit",
				'row' => $this->db_model->get_row('course',array('course_id' => $id))						
				);
														
		$this->load->view('administration/shared/master',$data);
	}
	
	public function add()
	{				
		$data = array(
				'error' => $this->error,
				'page_title' => "Course Management",
				'page_view' => "administration/pages/pg-course-edit",
				'mode' => "add",
				'row'=> $this->intialize_form()		
				);
														
		$this->load->view('administration/shared/master',$data);
	}	
//************** save *****		
	public function save()
	{
		if($this->input->post())
		{
			$this->load->library('form_validation');
			$this->form_validation->set_rules('course_title', 'Course Title', 'required');
			$this->form_validation->set_rules('course_code', 'Course Code', 'required');
	
			if ($this->form_validation->run() == FALSE)
			{
				$this->load_view();	
			}
			else
			{
				if($this->input->post('mode')=="edit")
				{
					$this->update();
				}
				else if($this->input->post('mode')=="add")
				{
					$this->insert();
				}	
			}
			
		}
		else
		{
			$this->add();
		}
	}
//************** delete *****	
	public function del($id)
	{
		
		$res = $this->db_model->delete_row("course",array('course_id'=>$id));
		
		if($res)
		{
			$this->session->set_flashdata('response', '<div class="success-box">Selected record has been deleted.</div>');
			redirect(base_url().'administration/course', 'refresh');
		}
		else
		{
			$this->session->set_flashdata('response', '<div class="error-box">Request can not be processed at the moment, please try again later.</div>');
			redirect(base_url().'administration/course', 'refresh');
		}
	}
	
//************** Update *****	
	public function update()
	{
		$vals = $this->input->post();
		unset($vals['btnSubmit'],$vals['mode'],$vals['id']);	
		
		$vals['assignment_weight']=($vals['assignment_weight'])/100;
		$vals['invitation_weight']=($vals['invitation_weight'])/100;
		$vals['question_weight']=($vals['question_weight'])/100;
			
		$vals['last_modified'] = date('Y-m-d h:i:s');
		
		//var_dump($vals);	
		//exit;			
		$where = array('course_id' => $this->input->post('id'));
		
		$res = $this->db_model->update_row('course',$vals,$where);
		
		if($res)
		{
			$this->session->set_flashdata('response', '<div class="success-box">Information has been modified.</div>');
			redirect(base_url().'administration/course/edit/'.$this->input->post('id').'', 'refresh');
		}
		else
		{
			$this->session->set_flashdata('response', '<div class="error-box">Request can not be processed at the moment, please try again later.</div>');
			redirect(base_url().'administration/course/edit/'.$this->input->post('id').'', 'refresh');
		}
	}
//************** Insert *****		
	public function insert()
	{
		
		$vals = $this->input->post();		
		unset($vals['btnSubmit'],$vals['mode'],$vals['id']);
		
		$vals['assignment_weight']=($vals['assignment_weight'])/100;
		$vals['invitation_weight']=($vals['invitation_weight'])/100;
		$vals['question_weight']=($vals['question_weight'])/100;
		
		$vals['last_modified'] = date('Y-m-d h:i:s');				
		
		$ret_id = $this->db_model->insert_row_retid("course",$vals);
		
		if($ret_id>0)
		{						
			$this->session->set_flashdata('response', '<div class="success-box">Information has been added.</div>');
			redirect(base_url().'administration/course/add', 'refresh');
		}
		else
		{
			$this->session->set_flashdata('response', '<div class="error-box">Request can not be processed at the moment, please try again later.</div>');
			redirect(base_url().'administration/course/add', 'refresh');
		}
	}

//************** Load View *****		
	private function load_view()
	{
		if($this->input->post('mode') == 'add')
		{
			$this->add();
			
		}
		else if($this->input->post('mode') == 'edit')
		{
			$this->edit($this->input->post('id'));		
		}
	}	
//************** initialize form *****		
	private function intialize_form()
	{
		$values = (object) array(
				 'course_id' => '',
				 'course_title' => '',
				 'course_code' => '',
				 'course_credit' => '',			 
				);
						
		return $values;
	}	
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */