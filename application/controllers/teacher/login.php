<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 
	public function __construct()
    {
		parent::__construct();				
		// Your own constructor code    	
	}	
	 
	public function index()
	{
		$data = array(
				'page_title' => "Teacher Login",
				'page_view' => "teacher/pages/pg-login"				
				);
														
		$this->load->view('teacher/shared/master',$data);
	}
	
	public function authenticate()
	{
		
		$vals = $this->input->post();
		
							
		$user = $this->db_model->get_row("teacher",$vals);
		
		if($user)
		{
			/*if($user->status == 0)
			{
				$this->session->set_flashdata('response', '<div class="error-box">Your account status is inactive now.</div>');			
				redirect(base_url().'administration/login', 'refresh');	
			}
			else
			{
				$this->session->set_userdata('loggedinteacher', $user);
				redirect(base_url().'teacher/cpanel', 'refresh');	
			}*/
			
			$this->session->set_userdata('loggedinteacher', $user);
				redirect(base_url().'teacher/cpanel', 'refresh');	
		}
		else
		{
			$this->session->set_flashdata('response', '<div class="error-box">Invalid credentials, please try again.</div>');
			redirect(base_url().'teacher/login', 'refresh');
		}			
						
	}
	
	public function logoff()
	{		
		$this->session->unset_userdata('loggedinteacher');
		redirect(base_url().'teacher/login', 'refresh');									
	}		
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */