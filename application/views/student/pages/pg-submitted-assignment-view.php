<script type="text/javascript" charset="utf-8">
$m(document).ready(function() {
	 initDataTables();
});	
	
function initDataTables(){	
 $m('#example').dataTable( {

		"bJQueryUI": true,	
		"aLengthMenu": [[10, 25 , 50, 100, -1],[10, 25 , 50, 100, "All"]],     
		"sPaginationType": "full_numbers",
		"bProcessing": true,
		"bStateSave": true,			
		"bAutoWidth": false,		
		"sDom": '<"H"CTlfr>t<"F"ilp>',
		
		"oTableTools": 
			{
				"aButtons": 
				[
					{						
						"sExtends":    "text",
						"sButtonText": "Due Assignments",						
						"fnClick": function ( nButton, oConfig, oFlash ) 
						{
							window.location = '<?PHP echo base_url();?>student/assignment/'
						}
					}
				]			
		},  				
				
		"aoColumns": [
						{ "sWidth": "auto"},  
						
						{ "sWidth": "auto"},
						{ "sWidth": "40"},
						
						{ "sWidth": "auto" },
						{ "sWidth": "auto" }
					 ],					
		
		"fnDrawCallback": function() 
		{},				
		
		"sAjaxSource": "<?PHP echo base_url()?>student/assignment/view_table/"
		
	} );	
			
}	
function cnfrm()
{
	return confirm('Are you sure you want to continue?');
}
</script>
    <fieldset>
    <legend><h2><?php echo $page_title; ?></h2></legend>  
  
    <table id="example" width="100%" cellpadding="0" cellspacing="0" class="dataTableGridNJ">    
    <thead>    
        <tr>
           
            <th align="left"><strong>Title</strong></th>
           
            <th align="left"><strong>Course</strong></th>
            <th align="left"><strong>Section</strong></th>
           
            <th align="left"><strong>Submission Date</strong></th>  
        
            <th align="left"><strong>Submission Format</strong></th> 
             
          
        </tr>
    </thead>
    <!--<tfoot>
        <tr>    	
            <th align="left">First Name</th>
            <th align="left">Last Name</th>
            <th align="left">Email</th>
            <th align="left">Website</th>
            <th align="left"></th>
        </tr>
    </tfoot>-->
    </table>
    </fieldset>
