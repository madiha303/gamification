

<script>

$(document).ready(function() {

   $('#textFormat').hide();
   $('#attachmentFormat').hide();
    
	var test=$('#frmts').val();
    
   if (test=="Attachment Only")
   {
	    $('#textFormat').hide();
   		$('#attachmentFormat').show();
		 
   }
   
   else if (test=="Text and Attachment")
   {
	    $('#textFormat').show();
  		 $('#attachmentFormat').show();
		 
   }
   else if (test=="Text Only")
   {
	    $('#textFormat').show();
  		$('#attachmentFormat').hide();
		
   }
   
   /* $("#btnSubmit").click(function(){
		var thought = $('textarea[name=content]').val();
  alert(thought);
  });*/

});			
</script>

<?php

if(isset($row))
{
	
	foreach($row as $rows){
	$value_id=$rows->assignment_id;
	$value_title=$rows->title;
	$value_total_marks=$rows->total_marks;
	$value_performance_marks=$rows->performance_marks;
	$value_subtotal=$value_total_marks+$value_performance_marks;
	$value_detail=$rows->detail;
	$value_course=$rows->course_title; 
	$value_open_date=date("F j, Y, g:i a",strtotime($rows->open_date));
	$value_due_date=date("F j, Y, g:i a",strtotime($rows->due_date));
	$value_accept_until_date=date("F j, Y, g:i a",strtotime($rows->accept_until_date));
	$value_attachment=$rows->attachment;
	$value_submission_format=$rows->submission_format;
	//var_dump ($rows);
	echo "<br/><br/><br/>";
	
	//var_dump('<br/>'.$rows->title);
	}
	
	
}
//var_dump($row_course);
$options = array('Text Only'=>'Text Only','Attachment Only'=>'Attachment Only','Text and Attachment'=>'Text and Attachment');
?>



<form id="assignmentFrm" name="assignmentFrm"  enctype="multipart/form-data" method="post" action="<?PHP echo base_url();?>student/assignment/insert">
<fieldset>
<legend><h2><?php echo $page_title; ?></h2></legend>
<table width="1209"  cellpadding="0" cellspacing="0">
	<tr>
    	<td><?php echo validation_errors('<div style="color:red;">','</div>'); ?></td>
    </tr>
	<tr>
    	<td>
        	<input type="hidden" name="frmts" id="frmts" value="<?PHP echo $value_submission_format;?>"/>
        	
            <p>
                <strong>Course:</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;       &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?= ucwords($value_course); ?>                           
        	</p>
            
            <p>
                <strong>Assignment Title:</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;<?= ucwords($value_title); ?>                           
        	</p>
            
             <p>
                <strong>Performance Marks:</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?= ucwords($value_performance_marks); ?>                           
        	</p>
            
             <p>
                <strong>Assignment Marks:</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?= ucwords($value_total_marks); ?>                           
        	</p>
            
            <p>
                <strong>Total Marks:</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?= ucwords($value_subtotal); ?>                           
        	</p>
            
            <p>
                <strong>Details:</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?= ucwords($value_detail); ?>               
        	</p>
            
            <p>              	
                <strong>Attachement: </strong>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                
               <?php 
			   if($value_attachment !=""){
				   $file_path= base_url()."assets/announcements/".$value_attachment;
				   ?>
              
                <?php echo $value_attachment;?>&nbsp;<a href="<?PHP echo $file_path;?>" target="_blank"> Download </a>
              
                    <?PHP }else echo "No Attachment ";?>
            </p>
          
          	<p>
              <strong>Open Date:</strong>
			  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			  <?= $value_open_date;?>   
                
              </p>
              
              <p>
              <strong>Due Date:</strong>
			  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			  <?= $value_due_date;?>   
                
              </p>
              
              <p>
              <strong>Accept Until Date:</strong>
			  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			  <?= $value_accept_until_date;?></p>
              
             <p>
                 <strong>Submission Format:</strong>
                 
                 &nbsp;&nbsp;
                  <?php echo $value_submission_format;?>
                            
              </p>
              
              <p>
                
                 
                 &nbsp;&nbsp;
                  
              </p>
              
              <div id="attachmentFormat" style="display:none;">
                  <p>
                    <label><strong>Upload File: (<?PHP echo $this->config->item('files_types');?>):</strong></label>
                <input size="62" name="attachment" id="attachment" type="file"  class="validate[required]" value="" />
                 </p>
              </div>
              
              <div id="textFormat" style="display:none;">
                  <p>
                    <label for="content"><strong>Description/Detail:</strong></label>                    
                    <textarea name="content" id="content" cols="" rows="" style="width:83%;height:400px;" class="validate[required]" > </textarea >
                 </p>
              </div>
              
              
              
             <p>
             	<input type="submit" name="btnSubmit" id="btnSubmit" value="Submit" /> 
                <input type="button" onclick="window.location='<?PHP echo base_url();?>student/assignment'" value="Cancel"/>               
             </p>
           
            
            
        </td>
    </tr>
</table>
<input type="hidden" name="is_graded" id="is_graded" value="0"/>
<input type="hidden" name="assignment_id" id="assignment_id" value="<?PHP if(isset($row)){echo $value_id;}?>"/>
<input type="hidden" name="open_date" id="open_date" value="<?PHP if(isset($row)){echo $value_open_date;}?>"/>
<input type="hidden" name="due_date" id="due_date" value="<?PHP if(isset($row)){echo $value_due_date;}?>"/>
<input type="hidden" name="accept_until_date" id="accept_until_date" value="<?PHP if(isset($row)){echo $value_accept_until_date;}?>"/>
<input type="hidden" name="obtained_performance_marks" id="obtained_performance_marks" value="<?PHP if(isset($row)){echo $value_performance_marks;}?>"/>


</fieldset>
</form>

