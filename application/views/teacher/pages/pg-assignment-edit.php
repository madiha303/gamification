<script>

$(function(){
	//$('#accept_until_date').datetimepicker({timeFormat: "hh:mm tt"});
	var startDateTextBox = $('#open_date');
	var endDateTextBox = $('#due_date');
	var lastDateTextBox= $('#accept_until_date');

	startDateTextBox.datetimepicker(
	{ 
		onClose: function(dateText, inst) 
		{
			if (endDateTextBox.val() != '')
			 {
				var testStartDate = startDateTextBox.datetimepicker('getDate');
				var testEndDate = endDateTextBox.datetimepicker('getDate');
				if (testStartDate > testEndDate)
					endDateTextBox.datetimepicker('setDate', testStartDate);
			}
			else {
				endDateTextBox.val(dateText);
			}
		},
		onSelect: function (selectedDateTime)
		{
			endDateTextBox.datetimepicker('option', 'minDate', startDateTextBox.datetimepicker('getDate') );
		}
	});
	
	endDateTextBox.datetimepicker(
	{ 
		onClose: function(dateText, inst) 
		{
			if (startDateTextBox.val() != '') 
			{
				var testStartDate = startDateTextBox.datetimepicker('getDate');
				var testEndDate = endDateTextBox.datetimepicker('getDate');
				if (testStartDate > testEndDate)
					startDateTextBox.datetimepicker('setDate', testEndDate);
			}
			else
			 {
				startDateTextBox.val(dateText);
			}
		},
		onSelect: function (selectedDateTime){
			startDateTextBox.datetimepicker('option', 'maxDate', endDateTextBox.datetimepicker('getDate') );
			lastDateTextBox.datetimepicker('option', 'minDate', endDateTextBox.datetimepicker('getDate') );
		}
	});
	
	
	lastDateTextBox.datetimepicker(
	{ 
		onClose: function(dateText, inst) 
		{
			if (endDateTextBox.val() != '')
			 {
				var testLastDate = lastDateTextBox.datetimepicker('getDate');
				var testEndDate = endDateTextBox.datetimepicker('getDate');
				if (testLastDate < testEndDate)
					endDateTextBox.datetimepicker('setDate', testLastDate);
			}
			else {
				endDateTextBox.val(dateText);
			}
		},
		onSelect: function (selectedDateTime)
		{
			endDateTextBox.datetimepicker('option', 'maxDate', lastDateTextBox.datetimepicker('getDate') );
		}
	});

	
});



function countChars() {
     var l = "1000";
     var str = document.getElementById("detail").value;
     var len = str.length;
     if(len <= l) {
          document.getElementById("txtLen").value=l-len;
     } else {
          document.getElementById("detail").value=str.substr(0, 1000);
     }
}

			
</script>
<?php

if(isset($row))
{
	//var_dump($row);
	foreach($row as $rows){
	$value_id=$rows->assignment_id;
	$value_title=$rows->title;
	$value_total_marks=$rows->total_marks;
	$value_detail=$rows->detail;
	$value_course=$rows->course_title.'   ('.$rows->section.')'; 
	$value_open_date=date('m/d/Y H:i', strtotime($rows->open_date));
	$value_due_date=date('m/d/Y H:i', strtotime($rows->due_date));
	$value_accept_until_date=date('m/d/Y H:i', strtotime($rows->accept_until_date));
	$value_attachment=$rows->attachment;
	$value_submission_format=$rows->submission_format;
	$value_performance_marks=$rows->performance_marks;
	//var_dump('<br/>'.$rows->title);
	}
	
	
}
//var_dump($row_course);
$options = array('Text Only'=>'Text Only','Attachment Only'=>'Attachment Only','Text and Attachment'=>'Text and Attachment');
?>

<form id="assignmentFrm" name="assignmentFrm"  enctype="multipart/form-data" method="post" action="<?PHP echo base_url();?>teacher/assignment/save">
<fieldset>
<legend><h2><?php echo $page_title; ?></h2></legend>
<table  cellpadding="0" cellspacing="0">
	<tr>
    	<td><?php echo validation_errors('<div style="color:red;">','</div>'); ?></td>
    </tr>
	<tr>
    	<td>
        	<p>
                <label><strong>Title:</strong></label>                    
                <input size="60" name="title" id="title" type="text" value="<?PHP if(isset($row)){echo set_value('title',$value_title);}?>" class="validate[required]"  />
        	</p>
        	
            <p>
        	  <label><strong>Detail:</strong></label>
        	 <textarea name="detail" id="detail" class="validate[required]" onKeyDown="countChars()" onKeyUp="countChars()" style="width:380px; height:120px; resize:none; text-transform:none; "><?PHP if(isset($row)){echo set_value('title',$value_detail);}?> </textarea>
            <INPUT  disabled size="4" value="1000" name="txtLen" id="txtLen" style="border:0px; background:none;"> Characters Left
      	  </p>  
          
          	<!--<p>
       	    <label><strong>Access:</strong></label>
       	    <label for="select"></label>
       	    <select name="access" id="access" style="width:380px;" >      
            	<?php foreach($options as $value=>$text):?>                                     
                <option value="<?php echo $value ?>" 
                        <?php if ($row->access == $value)
						{
							echo 'selected';
							}; ?> >
                        <?php echo $text ?>
                </option>
            	<?php endforeach; ?>
            </select>
          </p>  -->
             
            <p>
              <label><strong>Status:</strong></label>                    
            
                <input type="radio" name="status" value="Unpublished" class="validate[required] radio" 
				<?php if(isset($row)){ echo ($rows->status=='Unpublished')? 'checked' : '';} ?>  >Unpublished (Save As Draft )<br/>
                <input type="radio" name="status" value="Published" class="validate[required] radio" 
                <?php if(isset($row)){ echo ($rows->status=='Published')? 'checked' : '';} ?> >Published ( Display on Website )
          </p>          
             
            <p>
       	    <label><strong>Course:</strong></label>
       	    <label for="select"></label>
       	    <select name="course_id" id="course_id" style="width:382px;"  class="dropdown validate[required]">      
						 <option value="" <?php echo set_select('course_id', '', TRUE); ?>>Select Course</option>
						<?php foreach($row_course as $rows): 
						 $key1=$rows->course_id;
						 
						 	
                         $value=$rows->course_title.'   ('.$rows->section.')'; 
						
						 ?>                                              
                       
                        <option value="<?php echo $key1?>" 
                              <?php  
							   if(isset($row))
								{
								  	if ($value==$value_course)
									{
										echo 'selected';
									}; 
								}
								?>
                          >
                                <?php  echo $value ?>
                        </option>
                       
                        <?php endforeach; ?>
                        
            	</select>
          </p>
          
          	<p>
                <label><strong>Open Date:</strong></label>                    
                <input  name="open_date" id="open_date" type="text" class="validate[required]" style="width:173px;" value="<?PHP if(isset($row)){echo set_value('begining_date',$value_open_date);}?>"  /> 
                
              </p>
              <p>  <label><strong>Due Date:</strong></label>   
                <input  name="due_date" id="due_date" type="text" class="validate[required]" style="width:173px;" value="<?PHP if(isset($row)){echo set_value('begining_date',$value_due_date);}?>" />
             </p>
             	
               <p>  <label><strong>Accept Until Date:</strong></label>   
                <input  name="accept_until_date" id="accept_until_date" type="text" class="validate[required]" style="width:173px;" value="<?PHP if(isset($row)){echo set_value('begining_date',$value_accept_until_date);}?>" />
             </p>
              
             
              <p>              	
                <label><strong>Attachement: (<?PHP echo $this->config->item('files_types');?>):</strong></label>
                <input size="48" name="attachment" id="attachment" type="file" value="<?PHP if(isset($row)){echo set_value('attachment',$value_attachment);}?>"/>
               <?PHP if(isset($row)){
			   if($value_attachment !=""){
				   $file_path= base_url()."assets/announcements/".$value_attachment;
				   ?>
              <p>
                <?php echo $value_attachment;?>&nbsp;<a href="<?PHP echo $file_path;?>" target="_blank"> Download </a>
              </p>
                    <?PHP }}?>
                  </p>
          
             
		
            
            <p>
                <label><strong>Performance Marks:</strong></label>                    
                <input size="60" name="performance_marks" id="performance_marks" type="text" value="<?PHP if(isset($row)){echo set_value('performance_marks',$value_performance_marks);}else echo 0;?>" class="validate[required,custom[integer]] text-input"  />
        	</p>
            
            <p>
                <label><strong>Assignment Marks:</strong></label>                    
                <input size="60" name="total_marks" id="total_marks" type="text" value="<?PHP if(isset($row)){echo set_value('title',$value_total_marks);}?>" class="validate[required,custom[integer]] text-input"  />
        	</p>
             
           <p>
        	  <label><strong>Submission Format:</strong></label>
              <select name="submission_format" id="submission_format" style="width:382px;" class="dropdown validate[required]" >  
               <option value="" <?php echo set_select('submission_format', '', TRUE); ?>>Select Format</option>    
            	
				<?php foreach($options as $value=>$text):?>                                     
                <option value="<?php echo $value ?>" 
                         <?php  
							   if(isset($row))
								{
								  	if ($value==$value_submission_format)
									{
										echo 'selected';
									}; 
								}
								?>
                          >
                                <?php  echo $value ?>
                </option>
            	<?php endforeach; ?>
            </select>
      	  </p>
             
             <p>
             	<input type="submit" name="btnSubmit" id="btnSubmit" value="Save" /> 
                <input type="button" onclick="window.location='<?PHP echo base_url();?>teacher/assignment'" value="Cancel"/>               
             </p>
        </td>
    </tr>
</table>
<input type="hidden" name="mode" id="mode" value="<?PHP echo $mode;?>"/>
<input type="hidden" name="id" id="id" value="<?PHP if(isset($row)){echo $value_id;}?>"/>


</fieldset>
</form>

