<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Announcement extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 
	private $error = "";
	 
    public function __construct()
    {
		parent::__construct();
		
		if(!$this->session->userdata('loggedinuser'))
		{
			$this->session->set_flashdata('response', '<div class="error-box">Please login...!</div>');
			redirect(base_url().'administration/login', 'refresh');		
			exit;
		}	
		
		// Your own constructor code    	
	}	
	 
	public function index()
	{
          
            $data = array(
				'page_title' => "Announcement Management",
				'page_view' => "administration/pages/pg-announcement-view"
				);
														
		$this->load->view('administration/shared/master',$data);
	}
//************** Get Table *****		
	public function get_table()
	{
		$res =$this->db_model->get_table('announcement');
        echo "{ \"aaData\": [";
		if ($res)
        {
			 $indx = 1;
			 foreach ($res as $row)
             {
				 
				 $indx_id = $row->id;
				 
				 $edit_url = "<a href='".base_url()."administration/announcement/edit/".$indx_id."'><img src='".base_url()."assets/images/administration/icons/edit.gif'/></a>";
				 
				 $del_url = "<a href='".base_url()."administration/announcement/del/".$indx_id."' onclick='return cnfrm()'><img src='".base_url()."assets/images/administration/icons/del.gif'/></a>";	
					 
				 
				 $options = $edit_url." | ".$del_url;
	
				 
				 if ($indx != sizeof($res))
                 {
					  echo '["'.$row->title.'","'.$row->detail.'","'.$row->access.'","'.date("F j, Y, g:i a",strtotime($row->begining_date)).'","'.date("F j, Y, g:i a",strtotime($row->ending_date)).'","'.date("F j, Y, g:i a",strtotime($row->last_modified)).'","'.$row->status.'","'.$options.'"],';
				 }
				 else
				{
					 echo '["'.$row->title.'","'.$row->detail.'","'.$row->access.'","'.date("F j, Y, g:i a",strtotime($row->begining_date)).'","'.date("F j, Y, g:i a",strtotime($row->ending_date)).'","'.date("F j, Y, g:i a",strtotime($row->last_modified)).'","'.$row->status.'","'.$options.'"]';
				 }
				 
				 $indx++;
			 }
		}
		
		echo "] }";
			
	}	
//************** edit *****	
	
	public function edit($id)
	{
		$data = array(
				'error' => $this->error,
				'page_title' => "announcement Management",
				'page_view' => "administration/pages/pg-announcement-edit",
				'mode' => "edit",
				'row' => $this->db_model->get_row('announcement',array('id' => $id))						
				);
														
		$this->load->view('administration/shared/master',$data);
	}
	
	public function add()
	{				
		$data = array(
				'error' => $this->error,
				'page_title' => "Announcemet Management",
				'page_view' => "administration/pages/pg-announcement-edit",
				'mode' => "add",
				'row_course' => $this->db_model->get_table('course'),
				'row'=> $this->intialize_form()		
				);
														
		$this->load->view('administration/shared/master',$data);
	}	
//************** save *****		
	public function save()
	{
		if($this->input->post())
		{
			$this->load->library('form_validation');
				$this->form_validation->set_rules('title', 'Title', 'required');
			$this->form_validation->set_rules('detail', 'Detail', 'required');
			
			if ($this->form_validation->run() == FALSE)
			{
				$this->load_view();	
			}
			else
			{
				if($this->input->post('mode')=="edit")
				{
					$this->update();
				}
				else if($this->input->post('mode')=="add")
				{
					$this->insert();
				}	
			}
			
		}
		else
		{
			$this->add();
		}
	}
//************** delete *****	
	public function del($id)
	{
		
		$res = $this->db_model->delete_row("announcement",array('id'=>$id));
		
		if($res)
		{
			$this->session->set_flashdata('response', '<div class="success-box">Selected record has been deleted.</div>');
			redirect(base_url().'administration/announcement', 'refresh');
		}
		else
		{
			$this->session->set_flashdata('response', '<div class="error-box">Request can not be processed at the moment, please try again later.</div>');
			redirect(base_url().'administration/announcement', 'refresh');
		}
	}
	
//************** Update *****	
	public function update()
	{
		$vals = $this->input->post();
		unset($vals['btnSubmit'],$vals['mode'],$vals['id']);	
		
		if($_FILES['attachment']['name'] != "")
		{
			$attachment = $this->upload();
			$vals['attachment'] = $attachment;
		}
			
		$vals['last_modified'] = date('Y-m-d h:i:s');
		$vals['begining_date'] = date('Y-m-d H:i:s', strtotime($vals['begining_date']));
		$vals['ending_date'] = date('Y-m-d H:i:s', strtotime($vals['ending_date']));
		
		//var_dump($vals);				
		//exit;				
		$where = array('id' => $this->input->post('id'));
		
		$res = $this->db_model->update_row('announcement',$vals,$where);
		
		if($res)
		{
			$this->session->set_flashdata('response', '<div class="success-box">Information has been modified.</div>');
			redirect(base_url().'administration/announcement/edit/'.$this->input->post('id').'', 'refresh');
		}
		else
		{
			$this->session->set_flashdata('response', '<div class="error-box">Request can not be processed at the moment, please try again later.</div>');
			redirect(base_url().'administration/announcement/edit/'.$this->input->post('id').'', 'refresh');
		}
	}
//************** Insert *****		
	public function insert()
	{
		
		$vals = $this->input->post();		
		unset($vals['btnSubmit'],$vals['mode'],$vals['id']);
		
		if($_FILES['attachment']['name'] != "")
		{
			$attachment = $this->upload();
			$vals['attachment'] = $attachment;
		}
		
		$vals['last_modified'] = date('Y-m-d h:i:s');
		$vals['begining_date'] = date('Y-m-d H:i:s', strtotime($vals['begining_date']));
		$vals['ending_date'] = date('Y-m-d H:i:s', strtotime($vals['ending_date']));				
		//var_dump($vals);
		//exit;
		$ret_id = $this->db_model->insert_row_retid("announcement",$vals);
		//var_dump($ret_id);
		if($ret_id>0)
		{						
			$this->session->set_flashdata('response', '<div class="success-box">Information has been added.</div>');
			redirect(base_url().'administration/announcement/add', 'refresh');
		}
		else
		{
			$this->session->set_flashdata('response', '<div class="error-box">Request can not be processed at the moment, please try again later.</div>');
			redirect(base_url().'administration/announcement/add', 'refresh');
		}
	}
//************** Get image *****		
	public function get_image($img_name="",$width="",$height="")
	{
		if($img_name == "" || $width <=0 || $height <=0)
		{
			exit;
		}
		else
		{
			$img_name = str_replace("%20"," ",$img_name);
			$img = "./assets/students/".$img_name."";
			$this->image_moo->load($img)->resize($width,$height)->save_dynamic();
			if ($this->image_moo->errors) print $this->image_moo->display_errors(); 		
		}		
	}
	
//**************Upload Path
	private function upload($field = 'attachment')
	{
		$path = './assets/announcements/';
		$config['upload_path'] = $path;
		$config['allowed_types'] = $this->config->item('files_types');
		$config['max_size']	= '3072';
		$config['remove_spaces'] = true;
		$config['encrypt_name'] = false;
		
		$this->load->library('upload', $config);
		$this->upload->initialize($config);

		if ( ! $this->upload->do_upload($field))
		{
			$this->error = $this->upload->display_errors('', '<br/>');
			$this->load_view();
			die($this->output->get_output());									
		}
		else
		{						
			$data = $this->upload->data();
			//$this->image_moo->load($path.$data['file_name']."")->resize("800","640")->save_pa($prepend="", $append="", $overwrite=FALSE);
			return $data['file_name'];
		}
	}
	
//************** Load View *****		
	private function load_view()
	{
		if($this->input->post('mode') == 'add')
		{
			$this->add();
			
		}
		else if($this->input->post('mode') == 'edit')
		{
			$this->edit($this->input->post('id'));		
		}
	}	
//************** initialize form *****		
	private function intialize_form()
	{
		$values = (object) array(
				 'id' => '',
				 'title' => '',
				 'detail' => '',
				 'access' => '',	
				 'status' => '',	
				 'begining_date'=>'',
				 'ending_date'=>'',
				 'attachment'=>'',	
				  'notification'=>''	 
				);
						
		return $values;
	}	
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */