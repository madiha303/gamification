<table width="94%" border="0" cellspacing="0" cellpadding="0" class="textBlack">
                                   
                                    <tr> <td colspan="4">Complete the form and select appropiate action. <br>
                                      Fields marked with * are necessary</td>
                                    </tr>
                                    <tr>
                                      <td colspan="4">&nbsp;</td>
                                    </tr>
                                    <tr> <td colspan="4"><strong>Announcement Details:</strong></td>
                                    </tr>
                                    <tr>
                                      <td width="5%">&nbsp;</td>
                                      <td width="26%">&nbsp;Announcement Title *</td>
                                      <td width="50%"><input name="textfield" type="text" id="textfield" class="textField"/></td>
                                      <td width="19%">&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td align="right">&nbsp;</td>
                                      <td> Announcement Body *</td>
                                      <td><textarea name="textarea" id="textarea" cols="45" rows="5" class="textField"></textarea></td>
                                    </tr>
                                    <tr>
                                      <td>&nbsp;</td>
                                      <td>&nbsp;</td>
                                      <td>&nbsp;</td>
                                    </tr>
                                    <tr> <td colspan="4">&nbsp;</td>
                                    </tr>
                                    <tr> <td colspan="4"><strong>Access:</strong></td>
                                    </tr>
                                    <tr>
                                      <td>&nbsp;</td>
                                      <td colspan="2"><input type="radio" name="radio" id="radio5" value="radio" />
                                      Only <strong>members of this site</strong> can see this announcement</td>
                                    </tr>
                                    <tr>
                                      <td>&nbsp;</td>
                                      <td colspan="2"><input type="radio" name="radio" id="radio6" value="radio" />
                                      This announcement is <strong>publicly viewable</strong></td>
                                    </tr>
                                   
                                    <tr> <td colspan="4">&nbsp;</td>
                                    </tr>
                                    <tr> <td colspan="4"><strong>Availability :</strong></td>
                                    </tr>
                                    <tr>
                                      <td>&nbsp;</td>
                                      <td colspan="2"><label for="hidden_false">
                                        <input type="radio" name="radio" id="radio7" value="radio" />
                                        Show </label>
- (<strong>Post</strong> and display this announcement immediately)</td>
                                    </tr>
                                    <tr>
                                      <td>&nbsp;</td>
                                      <td colspan="2"><input type="radio" name="radio" id="radio8" value="radio" />
                                        <label for="hidden_true">Hide </label>
- (<strong>Draft mode</strong> - Do not display this announcement at this time)</td>
                                    </tr>
                                    <tr>
                                      <td>&nbsp;</td>
                                      <td colspan="2"><input type="radio" name="radio" id="radio9" value="radio" />
                                        <label for="hidden_specify">Specify Dates </label>
- (<strong>Choose when</strong> this announcement will be displayed)</td>
                                    </tr>
                                    <tr> <td colspan="4">&nbsp;</td>
                                    </tr>
                                    <tr> <td colspan="4">&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td>&nbsp;</td>
                                      <td><input type="checkbox" name="checkbox" id="checkbox" />
                                        Begining</td>
                                      <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td>&nbsp;</td>
                                      <td>Date </td>
                                      <td>Time</td>
                                    </tr>
                                    <tr>
                                      <td>&nbsp;</td>
                                      <td>&nbsp;</td>
                                      <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td>&nbsp;</td>
                                      <td><input type="checkbox" name="checkbox" id="checkbox" />
                                        Begining</td>
                                      <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td>&nbsp;</td>
                                      <td>Date </td>
                                      <td>Time</td>
                                    </tr>
                                    <tr>
                                      <td>&nbsp;</td>
                                      <td>&nbsp;</td>
                                      <td>&nbsp;</td>
                                    </tr>
                                    <tr> <td colspan="4">&nbsp;</td>
                                    </tr>
                                    <tr> <td colspan="4"><strong>Attachment:</strong></td>
                                    </tr>
                                    <tr>
                                      <td>&nbsp;</td>
                                      <td>&nbsp;</td>
                                      <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td>&nbsp;</td>
                                      <td><input type="radio" name="radio" id="radio" value="radio" onclick="document.getElementById('divAttachmentButton').style.visibility='visible';document.getElementById('divAddUrl').style.visibility='hidden';" />
                                      Upload Local File</td>
                                      <td>
                                        <div id="divAttachmentButton" style="visibility:hidden;">
                                          <input type="text" name="textfield3" id="textfield3" class="textField" />
                                          
                                          <input type="submit" name="button5" id="button5" value="Choose File" />
                                        </div>
                                      </td>
                                    </tr>
                                     
                                    
                                    
                                    <tr>
                                      <td>&nbsp;</td>
                                      <td><input type="radio" name="radio" id="radio2" value="radio" onclick="document.getElementById('divAddUrl').style.visibility='visible';document.getElementById('divAttachmentButton').style.visibility='hidden';" />
                                      Link to a website </td>
                                      <td>
                                      <div id="divAddUrl" style="visibility:hidden;">
                                      <input type="text" name="textfield2" id="textfield2" class="textField" />
                                      </div>
                                      </td>
                                    </tr>
                                    <tr>
                                      <td>&nbsp;</td>
                                      <td>&nbsp;</td>
                                      <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td>&nbsp;</td>
                                      <td>&nbsp;</td>
                                      <td>&nbsp;</td>
                                    </tr>
                                    <tr> <td colspan="4"><strong>Notification Priorty:</strong></td>
                                    </tr>
                                    <tr>
                                      <td>&nbsp;</td>
                                      <td colspan="2"><input type="radio" name="radio" id="radio4" value="radio" checked="checked" /> 
                                        Low- Send to those who have opted out</td>
                                    </tr>
                                    <tr>
                                      <td>&nbsp;</td>
                                      <td colspan="2"><input type="radio" name="radio" id="radio3" value="radio" />
                                      High- Send to all students</td>
                                    </tr>
                                    <tr>
                                      <td>&nbsp;</td>
                                      <td colspan="2"><input type="radio" name="radio" id="radio10" value="radio" />
                                      None- No notification </td>
                                    </tr>
                                    <tr>
                                      <td>&nbsp;</td>
                                      <td>&nbsp;</td>
                                      <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td>&nbsp;</td>
                                      <td>&nbsp;</td>
                                      <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td>&nbsp;</td>
                                      <td><input type="submit" name="button" id="button" value="Add Announcement" /></td>
                                      <td><input type="submit" name="button2" id="button2" value="Save as draft" />
                                      <input type="submit" name="button3" id="button3" value="Preview as Student" />                                        <input type="submit" name="button4" id="button4" value="Cancel" /></td>
                                      <td></td>
                                    </tr>
                                    <tr>
                                      <td>&nbsp;</td>
                                      <td>&nbsp;</td>
                                      <td>&nbsp;</td>
                                    </tr>
                                  </table>