<script type="text/javascript" charset="utf-8">
$m(document).ready(function() {
	 initDataTables();
});	
	
function initDataTables(){	
 $m('#example').dataTable( {

		"bJQueryUI": true,	
		"aLengthMenu": [[10, 25 , 50, 100, -1],[10, 25 , 50, 100, "All"]],     
		"sPaginationType": "full_numbers",
		"bProcessing": true,
		"bStateSave": true,			
		"bAutoWidth": false,		
		"sDom": '<"H"CTlfr>t<"F"ilp>',
		
		"oTableTools": 
			{
				"aButtons": 
				[
					{						
						"sExtends":    "text",
						"sButtonText": "Add New",						
						"fnClick": function ( nButton, oConfig, oFlash ) 
						{
							window.location = '<?PHP echo base_url();?>administration/teachercourse/add'
						}
					}
				]			
		},  				
				
		"aoColumns": [
						{ "sWidth": "auto"},  
						{ "sWidth": "auto"},
						{ "sWidth": "auto"},
						{ "sWidth": "auto"},
						{ "sWidth": "auto"},
						{ "sWidth": "auto"},
						{ "sWidth": "100","bSortable": false}                        
					
					 ],					
		
		"fnDrawCallback": function() 
		{},				
		
		"sAjaxSource": "<?PHP echo base_url()?>administration/teachercourse/get_table/"
		
	} );	
			
}	
function cnfrm()
{
	return confirm('Are you sure you want to continue?');
}
</script>
    <fieldset>
    <legend><h2><?php echo $page_title; ?></h2></legend>  
    <table id="example" width="100%" cellpadding="0" cellspacing="0" class="dataTableGridNJ">    
  
    <thead>    
        <tr>
        <th align="left"><strong>Department</strong></th> 
        <th align="left"><strong>Teacher Name</strong></th> 
        	<th align="left"><strong>Course Code</strong></th> 
            <th align="left"><strong>Course Name</strong></th> 
            <th align="left"><strong>Section</strong></th> 
            
                                           
            <th align="left"><strong>Last Modified</strong></th>    
            <th align="left"></th>    
            
        </tr>
    </thead>
 
    </tfoot>
    </table>
    </fieldset>
