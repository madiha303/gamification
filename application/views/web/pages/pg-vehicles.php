<!--//////////////// slider script ////////////////-->
<script type="text/javascript" charset="utf-8">
// <![CDATA[
$(document).ready(function(){	
	$("#slider").easySlider({
		controlsBefore:	'<p id="controls">',
		controlsAfter:	'</p>',
		auto: true, 
		continuous: true
	});	
});
// ]]>
</script>
<style type="text/css">
.gallery { background:#fff; border-bottom:1px solid #cdcdcd; width:993px; height:266px; margin:0 auto; padding:26px; }
#slider { margin:0; padding:0; list-style:none; }
#slider ul,
#slider li { margin:0; padding:0; list-style:none; }
/* 
    define width and height of list item (slide)
    entire slider area will adjust according to the parameters provided here
*/
#slider li { width:785px; height:231px; overflow:hidden; }
p#controls { margin:0; padding:0; position:relative; }
#prevBtn { display:block; margin:0; overflow:hidden; width:40px; height:47px; position:absolute; left:305px; top:65px; }
#nextBtn { display:block; margin:0; overflow:hidden; width:40px; height:47px; position:absolute; left: 435px; top:65px; }
#prevBtn a { display:block; width:43px; height:37px; background:url(<?PHP echo base_url()?>assets/images/arrow-left.png) no-repeat 0 0; }
#nextBtn a { display:block; width:43px; height:37px; background:url(<?PHP echo base_url()?>assets/images/arrow-right.png) no-repeat 0 0; }
</style>


<!--/////////////////////////////////////////////-->
<style type="text/css">
<!--
#slider {margin:0; padding:0; list-style:none; }
-->
</style>

<div id="slider">
  <ul>
 <?PHP 
 if($vehicles)
 {
 foreach($vehicles as $vehicle)
 {
  ?>
    <li>
      <table width="785" border="0" align="left" cellpadding="0" cellspacing="0">
        <tr>
          <td width="349" valign="top"><img src="<?PHP echo base_url();?>home/get_image/<?PHP echo $vehicle->photo;?>/313/227" /></td>
          <td width="436" valign="top"><table width="99%" border="0" align="left" cellpadding="0" cellspacing="0">
              <tr>
                <td height="29" align="left" valign="bottom" class="title"><?PHP echo $vehicle->title;?></td>
              </tr>
              <tr>
                <td height="11"></td>
              </tr>
              <tr>
                <td align="left" class="subtitle"><?PHP echo $vehicle->sub_title;?></td>
              </tr>
              <tr>
                <td height="11"></td>
              </tr>
              <tr>
                <td align="left" class="txt-13"> Contract details<br />
                  <?PHP echo substr($this->helper_model->remove_p($vehicle->contract_details),0,200);?></td>
              </tr>
              <tr>
                <td align="left">&nbsp;</td>
              </tr>
              <tr>
                <td align="left"><table width="91%" border="0" align="left" cellpadding="0" cellspacing="0">
                    <tr>
                      <td width="73%" align="left" class="txt-13-white">Contract length: <?PHP echo $vehicle->contract_length;?><br />
                        Initial payments: <?PHP echo $vehicle->initial_payments;?><br />
                        <a href="javascript:void(0);">Price per month: <?PHP echo $vehicle->monthly_price;?></a></td>
                      <td width="27%" align="right" class="requestQuote-link"><a href="#quote">REQUEST A QUOTE</a></td>
                    </tr>
                  </table></td>
              </tr>
            </table></td>
        </tr>
      </table>
    </li>
    <?PHP 
}
}
?>
  </ul>
</div>