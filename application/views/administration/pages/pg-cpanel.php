<fieldset>
<legend><h2><?php echo $page_title; ?></h2></legend>
    
    <table width="100%" align="center" cellpadding="0" cellspacing="0" >
      <tr>
        <td width="25%" align="left" style="border:none;" ><table align="center" cellpadding="0" cellspacing="0" style="border:none;">
          <tr>
            <td style="border:none;" align="center"><a href="<?php echo base_url(); ?>administration/student"> <img src="<?php echo base_url(); ?>assets/images/administration/icons/student-2.png" alt="" align="middle" border="0" /></a></td>
          </tr>
          <tr>
            <td style="border:none;" align="left"><font size="2"><strong>Student</strong></font></td>
          </tr>
          <tr></tr>
        </table></td>
               
      	<td width="25%" align="left" style="border:none;" ><table align="center" cellpadding="0" cellspacing="0" style="border:none;">
          <tr>
            <td style="border:none;" align="center"><a href="<?php echo base_url(); ?>administration/teacher"> <img src="<?php echo base_url(); ?>assets/images/administration/icons/teacher.png" alt="" align="middle" border="0" /></a></td>
          </tr>
          <tr>
            <td style="border:none;" align="left"><font size="2"><strong>Teacher</strong></font></td>
          </tr>
          <tr></tr>
        </table></td>
        
        <td width="25%" align="left" style="border:none;" ><table align="center" cellpadding="0" cellspacing="0" style="border:none;">
          <tr>
            <td style="border:none;" align="center"><a href="<?php echo base_url(); ?>administration/course"> <img src="<?php echo base_url(); ?>assets/images/administration/icons/book.png" alt="" align="middle" border="0" /></a></td>
          </tr>
          <tr>
            <td style="border:none;" align="left"><font size="2"><strong>Course</strong></font></td>
          </tr>
          <tr></tr>
        </table></td>
        
        
        <td width="25%" align="left" style="border:none;" ><table align="center" cellpadding="0" cellspacing="0" style="border:none;">
          <tr>
            <td style="border:none;" align="center"><a href="<?php echo base_url(); ?>administration/announcement"> <img src="<?php echo base_url(); ?>assets/images/administration/icons/announcement-icon-1.png" alt="" align="middle" border="0" /></a></td>
          </tr>
          <tr>
            <td style="border:none;" align="left"><font size="2"><strong>Announcement</strong></font></td>
          </tr>
          <tr></tr>
        </table></td>
        
        
        
        </tr>
      <tr>
        <td align="left" style="border:none;">&nbsp;</td>
        <td align="left" style="border:none;" >&nbsp;</td>
        <td align="left" style="border:none;" >&nbsp;</td>
        <td align="left" style="border:none;" >&nbsp;</td>
        <td align="left" style="border:none;" >&nbsp;</td>
      </tr>
      <tr>
        <td align="left" style="border:none;">&nbsp;</td>
        <td align="left" style="border:none;" >&nbsp;</td>
        <td align="left" style="border:none;" >&nbsp;</td>
        <td align="left" style="border:none;" >&nbsp;</td>
        <td align="left" style="border:none;" >&nbsp;</td>
      </tr>
      <tr>
      	<td width="21%" align="left" style="border:none;" ><table align="center" cellpadding="0" cellspacing="0" style="border:none;">
          <tr>
            <td style="border:none;" align="center"><a href="<?php echo base_url(); ?>administration/teachercourse"> <img src="<?php echo base_url(); ?>assets/images/administration/icons/assign-course.png" alt="" align="middle" border="0" /></a></td>
          </tr>
          <tr>
            <td style="border:none;" align="left"><strong><font size="2">Teacher-Courses</font></strong></td>
          </tr>
          <tr></tr>
        </table></td>
        
        <td width="21%" align="left" style="border:none;" ><table align="center" cellpadding="0" cellspacing="0" style="border:none;">
          <tr>
            <td style="border:none;" align="center"><a href="<?php echo base_url(); ?>administration/studentcourse"> <img src="<?php echo base_url(); ?>assets/images/administration/icons/assign-course-blue.png" alt="" align="middle" border="0" /></a></td>
          </tr>
          <tr>
            <td style="border:none;" align="left"><font size="2"><strong>Student-Courses</strong></font></td>
          </tr>
          <tr></tr>
        </table></td>
        
        <td width="21%" align="left" style="border:none;" ><table align="center" cellpadding="0" cellspacing="0" style="border:none;">
          <tr>
            <td style="border:none;" align="center"><a href="<?php echo base_url(); ?>administration/accountsettings"> <img src="<?php echo base_url(); ?>assets/images/administration/icons/account-settings.png" alt="" align="middle" border="0" /></a></td>
          </tr>
          <tr>
            <td style="border:none;" align="left"><font size="2"><strong>Account Settings</strong></font></td>
          </tr>
          <tr></tr>
        </table></td>
        
        <td align="left" style="border:none;"><table align="center" cellpadding="0" cellspacing="0" style="border:none;">
          <tr>
            <td style="border:none;" align="center"><a href="<?php echo base_url(); ?>administration/login/logoff"> <img src="<?php echo base_url(); ?>assets/images/administration/icons/Logout.png" alt="" align="middle" border="0" /></a></td>
          </tr>
          <tr>
            <td style="border:none;" align="left"><font size="2"><strong>Logout</strong></font></td>
          </tr>
          <tr></tr>
        </table></td>
        
        <td align="left" style="border:none;" >&nbsp;</td>
        <td align="left" style="border:none;" >&nbsp;</td>
        <td align="left" style="border:none;" >&nbsp;</td>
        </tr>
      <tr>
        <td align="left" style="border:none;">&nbsp;</td>
        <td align="left" style="border:none;" >&nbsp;</td>
        <td align="left" style="border:none;" >&nbsp;</td>
        <td align="left" style="border:none;" >&nbsp;</td>
        <td align="left" style="border:none;" >&nbsp;</td>
        </tr>
      </table>  
</fieldset>