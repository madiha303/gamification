<form id="accountSettingsFrm" name="accountSettingsFrm" method="post" action="<?PHP echo base_url();?>teacher/accountsettings/save">
<fieldset>
<legend><h2><?php echo $page_title; ?></h2></legend>
<table width="100%" cellpadding="0" cellspacing="0">
	<tr>
    	<td>
        
             <p>
                <label for="teacher_email"><strong>User Email:</strong></label>                    
                <input name="teacher_email" id="teacher_email" type="text" size="35" class="validate[required,custom[email]]" value="<?PHP echo $this->session->userdata('loggedinteacher')->teacher_email;?>" style="text-transform:none;" />
             </p>
             <p>
                <label for="teacher_password"><strong>Password:</strong></label>                    
                <input name="teacher_password" id="teacher_password" type="password" size="35" class="validate[required,minSize[5]]" value="<?PHP echo $this->session->userdata('loggedinteacher')->teacher_password;?>" style="text-transform:none;" />
             </p>
             
           <p>
                <label for="password1"><strong>ReType Password:</strong></label>                    
                <input type="password" size="35" class="validate[required,equals[teacher_password]]" value="<?PHP echo $this->session->userdata('loggedinteacher')->teacher_password;?>" style="text-transform:none;" />
             </p>
            
             <p>
             	<input type="submit" name="btnSubmit" id="btnSubmit" value="Save" />
                <input type="button" name="btnCancel" id="btnCancel" value="Cancel" onclick="window.location='<?PHP echo base_url();?>teacher/cpanel'" />
             </p>
        </td>
    </tr>
</table>
</fieldset>
</form>
