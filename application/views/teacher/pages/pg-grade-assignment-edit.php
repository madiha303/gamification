
<script>
$(document).ready(function(){
  $("#obtained_assignment_marks").focusout(function(){
	
	var test1= parseInt($("#obtained_assignment_marks").val());
	var test2=parseInt($("#obtained_performance_marks").val());
	var test=test1+test2;
   		
    //alert(test);
	 $("#sub_total_marks").val(test);
	 $('#sub_total_marks').attr('readonly', 'readonly');
  });
});
</script>
<?php

if(isset($row))
{
	//echo $mode;
	
	foreach($row as $rows){
	$value_id=$rows->assignment_id;
	$value_title=$rows->title;
	$value_total_marks=$rows->total_marks;
	$value_performance_marks=$rows->performance_marks;
	$value_subtotal=$value_total_marks+$value_performance_marks;
	$value_detail=$rows->detail;

	$value_open_date=date("F j, Y, g:i a",strtotime($rows->open_date));
	$value_due_date=date("F j, Y, g:i a",strtotime($rows->due_date));
	$value_accept_until_date=date("F j, Y, g:i a",strtotime($rows->accept_until_date));
	$value_attachment=$rows->attachment;
	$value_submission_format=$rows->submission_format;
	
	
	$value_submission_time=date("F j, Y, g:i a",strtotime($rows->submission_time));
	if($rows->is_graded==0)
					 {
						 $value_is_graded='No';
					 }
					 else
					 {
						 $value_is_graded='Yes';
					 }
	//$value_is_graded=$rows->is_graded;
	$value_accept_until_date=date("F j, Y, g:i a",strtotime($rows->accept_until_date));
	$value_content=$rows->content;
	$value_student_assignment_submission_id=$rows->student_assignment_submission_id;
	$value_obtained_assignment_marks=$rows->obtained_assignment_marks;
	$value_obtained_performance_marks=$rows->obtained_performance_marks;
	
	//var_dump ($rows);
	
	
	//echo "<br/><br/><br/>";
	
	//var_dump('<br/>'.$rows->title);
	}
	
	//echo $value_student_assignment_submission_id;
}
//var_dump($row_course);
$options = array('Text Only'=>'Text Only','Attachment Only'=>'Attachment Only','Text and Attachment'=>'Text and Attachment');
?>



<form id="assignmentFrm" name="assignmentFrm"  enctype="multipart/form-data" method="post" action="<?PHP echo base_url();?>teacher/submittedassignment/save">
<fieldset>
<legend><h2><?php echo $page_title; ?></h2></legend>
<table width="1209"  cellpadding="0" cellspacing="0">
	<tr>
    	<td><?php echo validation_errors('<div style="color:red;">','</div>'); ?></td>
    </tr>
	<tr>
    	<td>
        	
            
            <p>
                <strong>Assignment Title:</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;<?= ucwords($value_title); ?>                           
        	</p>
            
            <p>
                <strong>Details:</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?= ucwords($value_detail); ?>               
        	</p>
            
      <p>
                <strong>Assignment Marks:</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?= ucwords($value_total_marks); ?>                           
        	</p>
            
            <p>
                <strong>Performance Marks:</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?= ucwords($value_performance_marks); ?>                           
        	</p>
            
            
            
          
   	    <!--<p>
              <strong>Open Date:</strong>
			  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			  <?= $value_open_date;?>   
                
              </p>
              
              <p>
              <strong>Due Date:</strong>
			  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			  <?= $value_due_date;?>   
                
              </p>
              
              <p>
              <strong>Accept Until Date:</strong>
			  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			  <?= $value_accept_until_date;?></p>
              
             <p>
                 <strong>Submission Format:</strong>
                 
                 &nbsp;&nbsp;
                  <?php echo $value_submission_format;?>
                            
              </p>-->
              
              <p>               
          <hr>                
          </p>
              
              <p>
              <strong>Is Graded:</strong>
			  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			  <?= $value_is_graded;?></p>
              
              <p>
              <strong>Submission Time:</strong>
			  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			  <?= $value_submission_time;?></p>
              
              <p>              	
                <strong>Attachement: </strong>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                
               <?php 
			   if($value_attachment !=""){
				   $file_path= base_url()."assets/announcements/".$value_attachment;
				   ?>
              
                <?php echo $value_attachment;?>&nbsp;<a href="<?PHP echo $file_path;?>" target="_blank"> Download </a>
              
                    <?PHP }else echo "No Attachment ";?>
            </p>
              
              
        
            
              <fieldset> 
              <legend> Assignment Submission</legend> 
               <?= $value_content;?>
              </fieldset>
              
          <p>
                <label><strong>Obtained Performance Marks:</strong></label>                    
            <input size="60" name="obtained_performance_marks" id="obtained_performance_marks" type="text" value="<?PHP if(isset($row)){echo set_value('obtained_performance_marks',$value_obtained_performance_marks);}?>" readonly  />
        	 </p>
            
             <p>
                <label><strong>Obtained Assignment Marks:</strong></label>                    
                <input size="60" name="obtained_assignment_marks" id="obtained_assignment_marks" type="text" class="validate[required,custom[integer],max[<?= $value_total_marks; ?>]] text-input"  value="<?PHP if((isset($row))&&($value_obtained_assignment_marks!='')){echo set_value('obtained_assignment_marks',$value_obtained_assignment_marks);}?>"  />
        	 </p>

             <p>
                <label><strong>Total Marks:</strong></label>                    
                <input size="60" name="sub_total_marks" id="sub_total_marks" type="text"  />
        	 </p>
              
             <p>
           	   <input type="submit" name="btnSubmit" id="btnSubmit" value="Submit Grade" /> 
                <input type="button" onclick="window.location='<?PHP echo base_url();?>teacher/submittedassignment/edit/<?= $value_id;?>'" value="Cancel"/>               
             </p>
           
            
            
        </td>
    </tr>
</table>
<input type="hidden" name="is_graded" id="is_graded" value="1"/>
<input type="hidden" name="mode" id="mode" value="<?PHP echo $mode;?>"/>
<input type="hidden" name="assignment_id" id="assignment_id" value="<?PHP if(isset($row)){echo $value_id;}?>"/>
<input type="hidden" name="student_assignment_submission_id" id="student_assignment_submission_id" value="<?PHP if(isset($row)){echo $value_student_assignment_submission_id;}?>"/>
<!--<input type="hidden" name="open_date" id="open_date" value="<?PHP if(isset($row)){echo $value_open_date;}?>"/>
<input type="hidden" name="due_date" id="due_date" value="<?PHP if(isset($row)){echo $value_due_date;}?>"/>
<input type="hidden" name="obtained_performance_marks" id="obtained_performance_marks" value="<?PHP if(isset($row)){echo $value_performance_marks;}?>"/>
-->

</fieldset>
</form>

