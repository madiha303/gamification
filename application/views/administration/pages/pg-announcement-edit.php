<script>

$(function(){
	//$('#begining_date').datetimepicker({timeFormat: "hh:mm tt"});
	var startDateTextBox = $('#begining_date');
	var endDateTextBox = $('#ending_date');

	startDateTextBox.datetimepicker({ 
		onClose: function(dateText, inst) {
			if (endDateTextBox.val() != '') {
				var testStartDate = startDateTextBox.datetimepicker('getDate');
				var testEndDate = endDateTextBox.datetimepicker('getDate');
				if (testStartDate > testEndDate)
					endDateTextBox.datetimepicker('setDate', testStartDate);
			}
			else {
				endDateTextBox.val(dateText);
			}
		},
		onSelect: function (selectedDateTime){
			endDateTextBox.datetimepicker('option', 'minDate', startDateTextBox.datetimepicker('getDate') );
		}
	});
	endDateTextBox.datetimepicker({ 
		onClose: function(dateText, inst) {
			if (startDateTextBox.val() != '') {
				var testStartDate = startDateTextBox.datetimepicker('getDate');
				var testEndDate = endDateTextBox.datetimepicker('getDate');
				if (testStartDate > testEndDate)
					startDateTextBox.datetimepicker('setDate', testEndDate);
			}
			else {
				startDateTextBox.val(dateText);
			}
		},
		onSelect: function (selectedDateTime){
			startDateTextBox.datetimepicker('option', 'maxDate', endDateTextBox.datetimepicker('getDate') );
		}
	});

	
});



function countChars() {
     var l = "1000";
     var str = document.getElementById("detail").value;
     var len = str.length;
     if(len <= l) {
          document.getElementById("txtLen").value=l-len;
     } else {
          document.getElementById("detail").value=str.substr(0, 1000);
     }
}

			
</script>
<?php
$options = array('Public'=>'Public','Computer Science'=>'Computer Science','Software Engineering'=>'Software Engineering', 'Computer Arts'=>'Computer Arts');
$options1 = array('Unpublished'=>'Unpublished','Published'=>'Published');
$notifications=array('Low'=>'Low-->Send to those who opted','High'=>'High-->Send to all students','None'=>'None');
?>

<form id="announementFrm" name="announementFrm"  enctype="multipart/form-data" method="post" action="<?PHP echo base_url();?>administration/announcement/save">
<fieldset>
<legend><h2><?php echo $page_title; ?></h2></legend>
<table  cellpadding="0" cellspacing="0">
	<tr>
    	<td><?php echo validation_errors('<div style="color:red;">','</div>'); ?></td>
    </tr>
	<tr>
    	<td>
        	<p>
                <label><strong>Title:</strong></label>                    
                <input size="60" name="title" id="title" type="text" value="<?PHP echo set_value('title',$row->title);?>" class="validate[required]" style="text-transform:capitalize;" />
        	</p>
        	
            <p>
        	  <label><strong>Detail:</strong></label>
        	 <textarea name="detail" id="detail" class="validate[required]" onKeyDown="countChars()" onKeyUp="countChars()" style="width:380px; height:120px; resize:none;"><?PHP echo set_value('detail',$row->detail);?></textarea>
            <INPUT  disabled size="4" value="1000" name="txtLen" id="txtLen" style="border:0px; background:none;"> Characters Left
      	  </p>  
          
          	<!--<p>
       	    <label><strong>Access:</strong></label>
       	    <label for="select"></label>
       	    <select name="access" id="access" style="width:380px;" >      
            	<?php foreach($options as $value=>$text):?>                                     
                <option value="<?php echo $value ?>" 
                        <?php if ($row->access == $value)
						{
							echo 'selected';
							}; ?> >
                        <?php echo $text ?>
                </option>
            	<?php endforeach; ?>
            </select>
          </p>  -->
             
            <p>
              <label><strong>Status:</strong></label>                    
            
                <input type="radio" name="status" value="Unpublished" class="validate[required] radio" <?=($row->status=='Unpublished')? 'checked' : '' ?>  >Unpublished (Save As Draft )<br/>
                <input type="radio" name="status" value="Published" class="validate[required] radio" <?=($row->status=='Published')? 'checked' : '' ?> >Published ( Display on Website )
          </p>          
             
            <p>
       	    <label><strong>Notification Priorty:</strong></label>
       	    <label for="select"></label>
       	    <select name="notification" id="notification" style="width:380px;" >      
            	<?php foreach($notifications as $value=>$text):?>                                     
                <option value="<?php echo $value ?>" 
                        <?php if ($row->notification == $value)
						{
							echo 'selected';
							}; ?> >
                        <?php echo $text ?>
                </option>
            	<?php endforeach; ?>
            </select>
          </p>
          
          	<p>
                <label><strong>announcement Duration:</strong></label>                    
                <input  name="begining_date" id="begining_date" type="text" class="validate[required]" style="width:173px;" value="<?PHP if($row->begining_date!= NULL) { echo set_value('begining_date',date('m-d-Y H:i', strtotime($row->begining_date)));}?>"  /> to 
                <input  name="ending_date" id="ending_date" type="text" class="validate[required]" style="width:173px;" value="<?PHP if($row->ending_date!= NULL) {echo set_value('ending_date',date('m-d-Y H:i', strtotime($row->ending_date)));}?>" />
             </p>
             
              
             
              <p>              	
                <label><strong>Attachement: (<?PHP echo $this->config->item('files_types');?>):</strong></label>
                <input size="48" name="attachment" id="attachment" type="file" value="<?PHP echo set_value('title',$row->attachment);?>"/>
               <?PHP if($row->attachment !=""){
				   $file_path= base_url()."assets/announcements/".$row->attachment;
				   ?>
              <p>
                <a href="<?PHP echo $file_path;?>" target="_blank"> Download </a>
              </p>
                    <?PHP }?>
                  </p>
          
             

             
           
             
             <p>
             	<input type="submit" name="btnSubmit" id="btnSubmit" value="Save" /> 
                <input type="button" onclick="window.location='<?PHP echo base_url();?>administration/announcement'" value="cancel"/>               
             </p>
        </td>
    </tr>
</table>
<input type="hidden" name="mode" id="mode" value="<?PHP echo $mode;?>"/>
<input type="hidden" name="id" id="id" value="<?PHP if(isset($row)){echo $row->id;}?>"/>
<input type="hidden" name="is_admin" id="is_admin" value="1"/>
<input type="hidden" name="access" id="access" value="Public"/>
</fieldset>
</form>

