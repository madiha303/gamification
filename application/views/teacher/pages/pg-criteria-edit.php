<script>

$(function(){
	//$('#begining_date').datetimepicker({timeFormat: "hh:mm tt"});
	var startDateTextBox = $('#begining_date');
	var endDateTextBox = $('#ending_date');

	startDateTextBox.datetimepicker({ 
		onClose: function(dateText, inst) {
			if (endDateTextBox.val() != '') {
				var testStartDate = startDateTextBox.datetimepicker('getDate');
				var testEndDate = endDateTextBox.datetimepicker('getDate');
				if (testStartDate > testEndDate)
					endDateTextBox.datetimepicker('setDate', testStartDate);
			}
			else {
				endDateTextBox.val(dateText);
			}
		},
		onSelect: function (selectedDateTime){
			endDateTextBox.datetimepicker('option', 'minDate', startDateTextBox.datetimepicker('getDate') );
		}
	});
	endDateTextBox.datetimepicker({ 
		onClose: function(dateText, inst) {
			if (startDateTextBox.val() != '') {
				var testStartDate = startDateTextBox.datetimepicker('getDate');
				var testEndDate = endDateTextBox.datetimepicker('getDate');
				if (testStartDate > testEndDate)
					startDateTextBox.datetimepicker('setDate', testEndDate);
			}
			else {
				startDateTextBox.val(dateText);
			}
		},
		onSelect: function (selectedDateTime){
			startDateTextBox.datetimepicker('option', 'maxDate', endDateTextBox.datetimepicker('getDate') );
		}
	});

	
});



function countChars() {
     var l = "1000";
     var str = document.getElementById("detail").value;
     var len = str.length;
     if(len <= l) {
          document.getElementById("txtLen").value=l-len;
     } else {
          document.getElementById("detail").value=str.substr(0, 1000);
     }
}

			
</script>
<?php

if(isset($row))
{
	
	//var_dump($row);
	foreach($row as $rows){
	//$value_id=$rows->id;
	//$value_title=$rows->lecture_title;
	//$value_course=$rows->course_title.'   ('.$rows->section.')'; 
	//$value_attachment=$rows->attachment;
	
	}
	
	
}

?>

<form id="announementFrm" name="announementFrm"  enctype="multipart/form-data" method="post" action="<?PHP echo base_url();?>teacher/course/save">
<fieldset>
<legend><h2><?php echo $page_title; ?></h2></legend>
<table  cellpadding="0" cellspacing="0">
	<tr>
    	<td><?php echo validation_errors('<div style="color:red;">','</div>'); ?></td>
    </tr>
	<tr>
    	<td>
        	
        	<p>
                <label><strong>Assignment Weightage:</strong></label>                    
                <input size="60" name="assignment_weight" id="assignment_weight" type="text" value="<?PHP if(isset($row)){echo set_value('lecture_title',(($row->assignment_weight)*100));}?>" class="validate[required,custom[integer],max[20]] text-input" />
        	</p>
            
            <p>
                <label><strong>Question Pool Weightage:</strong></label>                    
                <input size="60" name="question_weight" id="question_weight" type="text" value="<?PHP if(isset($row)){echo set_value('lecture_title',(($row->question_weight)*100));}?>" class="validate[required,custom[integer],max[15]] text-input"  />
        	</p>
            
            <p>
                <label><strong>Invitation Weightage:</strong></label>                    
                <input size="60" name="invitation_weight" id="invitation_weight" type="text" value="<?PHP if(isset($row)){echo set_value('lecture_title',(($row->invitation_weight)*100));}?>" class="validate[required,custom[integer],max[15]] text-input" />
        	</p>
            
           
            
           
             
             <p>
             	<input type="submit" name="btnSubmit" id="btnSubmit" value="Save" /> 
                <input type="button" onclick="window.location='<?PHP echo base_url();?>teacher/course'" value="cancel"/>               
             </p>
        </td>
    </tr>
</table>
<input type="hidden" name="mode" id="mode" value="<?PHP echo $mode;?>"/>
<input type="hidden" name="id" id="id" value="<?PHP if(isset($row)){echo $row->course_id;}?>"/>
<input type="hidden" name="last_modified" id="last_modified" value=""/>

</fieldset>
</form>

