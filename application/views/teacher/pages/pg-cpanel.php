<fieldset>
<legend><h2><?php echo $page_title; ?></h2></legend>
    
    <table width="100%" align="center" cellpadding="0" cellspacing="0" >
      <tr>
        <td width="21%" align="left" style="border:none;" ><table align="center" cellpadding="0" cellspacing="0" style="border:none;">
          <tr>
            <td style="border:none;" align="center"><a href="<?php echo base_url(); ?>teacher/course"> <img src="<?php echo base_url(); ?>assets/images/teacher/icons/book.png" alt="" align="middle" border="0" /></a></td>
          </tr>
          <tr>
            <td style="border:none;" align="left"><font size="2"><strong>Courses</strong></font></td>
          </tr>
          <tr></tr>
        </table></td>
               
     	<td width="21%" align="left" style="border:none;" ><table align="center" cellpadding="0" cellspacing="0" style="border:none;">
          <tr>
            <td style="border:none;" align="center"><a href="<?php echo base_url(); ?>teacher/announcement"> <img src="<?php echo base_url(); ?>assets/images/teacher/icons/announcement-icon-1.png" alt="" align="middle" border="0" /></a></td>
          </tr>
          <tr>
            <td style="border:none;" align="left"><font size="2"><strong>Announcements</strong></font></td>
          </tr>
          <tr></tr>
        </table></td>
        
        <td width="21%" align="left" style="border:none;" ><table align="center" cellpadding="0" cellspacing="0" style="border:none;">
          <tr>
            <td style="border:none;" align="center"><a href="<?php echo base_url(); ?>teacher/lecture"> <img src="<?php echo base_url(); ?>assets/images/teacher/icons/assign-course-blue.png" alt="" align="middle" border="0" /></a></td>
          </tr>
          <tr>
            <td style="border:none;" align="left"><font size="2"><strong>Lecture</strong></font></td>
          </tr>
          <tr></tr>
        </table></td>
        
        <td width="21%" align="left" style="border:none;" ><table align="center" cellpadding="0" cellspacing="0" style="border:none;">
          <tr>
            <td style="border:none;" align="center"><a href="<?php echo base_url(); ?>teacher/assignment"> <img src="<?php echo base_url(); ?>assets/images/teacher/icons/assign-course.png" alt="" align="middle" border="0" /></a></td>
          </tr>
          <tr>
            <td style="border:none;" align="left"><font size="2"><strong>Assignments</strong></font></td>
          </tr>
          <tr></tr>
        </table></td>
        
        
        </tr>
      <tr>
        <td align="left" style="border:none;">&nbsp;</td>
        <td align="left" style="border:none;" >&nbsp;</td>
        <td align="left" style="border:none;" >&nbsp;</td>
        <td align="left" style="border:none;" >&nbsp;</td>
       
      </tr>
      <tr>
        <td align="left" style="border:none;">&nbsp;</td>
        <td align="left" style="border:none;" >&nbsp;</td>
        <td align="left" style="border:none;" >&nbsp;</td>
        <td align="left" style="border:none;" >&nbsp;</td>
       
      </tr>
      <tr>
      	<td width="21%" align="left" style="border:none;" ><table align="center" cellpadding="0" cellspacing="0" style="border:none;">
          <tr>
            <td style="border:none;" align="center"><a href="<?php echo base_url(); ?>teacher/questionpool"> <img src="<?php echo base_url(); ?>assets/images/teacher/icons/quotes-1.png" alt="" align="middle" border="0" /></a></td>
          </tr>
          <tr>
            <td style="border:none;" align="left"><font size="2"><strong>Question Pool</strong></font></td>
          </tr>
          <tr></tr>
        </table></td>
        
        <td width="21%" align="left" style="border:none;" ><table align="center" cellpadding="0" cellspacing="0" style="border:none;">
          <tr>
            <td style="border:none;" align="center"><a href="<?php echo base_url(); ?>teacher/gradebook"> <img src="<?php echo base_url(); ?>assets/images/teacher/icons/survey-manage.png" alt="" align="middle" border="0" /></a></td>
          </tr>
          <tr>
            <td style="border:none;" align="left"><font size="2"><strong>Mark Book</strong></font></td>
          </tr>
          <tr></tr>
        </table></td>
        
        <td width="21%" align="left" style="border:none;" ><table align="center" cellpadding="0" cellspacing="0" style="border:none;">
          <tr>
            <td style="border:none;" align="center"><a href="<?php echo base_url(); ?>teacher/accountsettings"> <img src="<?php echo base_url(); ?>assets/images/teacher/icons/account-settings.png" alt="" align="middle" border="0" /></a></td>
          </tr>
          <tr>
            <td style="border:none;" align="left"><font size="2"><strong>Account Settings</strong></font></td>
          </tr>
          <tr></tr>
        </table></td>
        
        <td align="left" style="border:none;"><table align="center" cellpadding="0" cellspacing="0" style="border:none;">
          <tr>
            <td style="border:none;" align="center"><a href="<?php echo base_url(); ?>teacher/login/logoff"> <img src="<?php echo base_url(); ?>assets/images/teacher/icons/Logout.png" alt="" align="middle" border="0" /></a></td>
          </tr>
          <tr>
            <td style="border:none;" align="left"><font size="2"><strong>Logout</strong></font></td>
          </tr>
          <tr></tr>
        </table></td>
        
        
        </tr>
      <tr>
        <td align="left" style="border:none;">&nbsp;</td>
        <td align="left" style="border:none;" >&nbsp;</td>
        <td align="left" style="border:none;" >&nbsp;</td>
        <td align="left" style="border:none;" >&nbsp;</td>
        
        </tr>
      </table>  
</fieldset>