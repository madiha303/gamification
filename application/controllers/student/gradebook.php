<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Gradebook extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 
	private $error = "";
	 
    public function __construct()
    {
		parent::__construct();
		
		if(!$this->session->userdata('loggedinstudent'))
		{
			$this->session->set_flashdata('response', '<div class="error-box">Please login...!</div>');
			redirect(base_url().'student/login', 'refresh');		
			exit;
		}	
		
		// Your own constructor code    	
	}	
	 
	public function index()
	{
          
            $data = array(
				'page_title' => "Grade Book Management",
				'page_view' => "student/pages/pg-cpanel-grade"
				);
														
		$this->load->view('student/shared/master',$data);
	}

public function index4()
	{
          
            $data = array(
				'page_title' => "Assignment Marks",
				'page_view' => "student/pages/pg-grade-view"
				);
														
		$this->load->view('student/shared/master',$data);
	}


//************** Get Table *****		
	public function get_assignment_table()
	{
		$where=array('student_assignment_submission.student_id'=>$this->session->userdata('loggedinstudent')->student_id,'student_assignment_submission.is_graded'=>1);
		
		$res=$this->db_model->select_multiple_joins_where_groupby(array('assignment.*','student_assignment_submission.*','course.*','student_assignment_submission.last_modified'),'assignment',$where,'student_assignment_submission','assignment_id','course','course_id','');
		
        echo "{ \"aaData\": [";
		if ($res)
        {
			 $indx = 1;
			 foreach ($res as $row)
             {
				 
				 $indx_id = $row->assignment_id;
			
				 if ($indx != sizeof($res))
                 {										 
					  echo '["'.$row->title.'","'.$row->course_title.'","'.date("F j, Y, g:i a",strtotime($row->submission_time)).'","'.$row->obtained_assignment_marks.'","'.$row->obtained_performance_marks.'","'.$row->sub_total_marks.'","'.date("F j, Y, g:i a",strtotime($row->last_modified)).'"],';
				 }
				 else
				{										
					 echo '["'.$row->title.'","'.$row->course_title.'","'.date("F j, Y, g:i a",strtotime($row->submission_time)).'","'.$row->obtained_assignment_marks.'","'.$row->obtained_performance_marks.'","'.$row->sub_total_marks.'","'.date("F j, Y, g:i a",strtotime($row->last_modified)).'"]';
				 }
				 
				 $indx++;
			 }
		}		
		echo "] }";
			
	}	
	
	
//*************
public function index2()
	{
          
            $data = array(
				'page_title' => "QuestionPool Marks",
				'page_view' => "student/pages/pg-grade-questionpool-view"
				);
														
		$this->load->view('student/shared/master',$data);
	}	
	
//************** Get Table *****		
	public function get_approved_table()
	{
		$where=array('student_questionpool_submission.student_id'=>$this->session->userdata('loggedinstudent')->student_id,'student_questionpool_submission.is_approved'=>1);
		
		$res=$this->db_model->select_multiple_joins_where_groupby(array('questionpool.*','student_questionpool_submission.*','course.*','student_questionpool_submission.last_modified'),'questionpool',$where,'student_questionpool_submission','questionpool_id','course','course_id','');
		
        echo "{ \"aaData\": [";
		if ($res)
        {
			 $indx = 1;
			 foreach ($res as $row)
             {
				 
				 $indx_id = $row->questionpool_id;
			
				 if ($indx != sizeof($res))
                 {										 
					  echo '["'.$row->title.'","'.$row->course_title.'","'.date("F j, Y, g:i a",strtotime($row->submission_time)).'","'.$row->total_marks.'","'.date("F j, Y, g:i a",strtotime($row->last_modified)).'"],';
				 }
				 else
				{										
					 echo '["'.$row->title.'","'.$row->course_title.'","'.date("F j, Y, g:i a",strtotime($row->submission_time)).'","'.$row->total_marks.'","'.date("F j, Y, g:i a",strtotime($row->last_modified)).'"]';
				 }
				 
				 $indx++;
			 }
		}		
		echo "] }";
			
	}	
	
	
	//*************
public function index3()
	{
          
            $data = array(
				'page_title' => "Invitation Marks",
				'page_view' => "student/pages/pg-grade-invitation-view"
				);
														
		$this->load->view('student/shared/master',$data);
	}	
	
//************** Get Table *****		
	public function get_approved_invitation_table()
	{
		$where=array('student_invitation_acceptance.student_id'=>$this->session->userdata('loggedinstudent')->student_id,'student_invitation_acceptance.is_accepted'=>1);
		
		$res=$this->db_model->select_multiple_joins_where_groupby(array('invitation.*','student_invitation_acceptance.*','course.*','student_invitation_acceptance.last_modified'),'invitation',$where,'student_invitation_acceptance','invitation_id','course','course_id','');
		
        echo "{ \"aaData\": [";
		if ($res)
        {
			 $indx = 1;
			 foreach ($res as $row)
             {
				 
				 $indx_id = $row->invitation_id;
			
				 if ($indx != sizeof($res))
                 {										 
					  echo '["'.$row->title.'","'.$row->course_title.'","'.date("F j, Y, g:i a",strtotime($row->submission_time)).'","'.$row->acceptance_marks.'","'.$row->attendance_marks.'","'.date("F j, Y, g:i a",strtotime($row->last_modified)).'"],';
				 }
				 else
				{										
					 echo '["'.$row->title.'","'.$row->course_title.'","'.date("F j, Y, g:i a",strtotime($row->submission_time)).'","'.$row->acceptance_marks.'","'.$row->attendance_marks.'","'.date("F j, Y, g:i a",strtotime($row->last_modified)).'"]';
				 }
				 
				 $indx++;
			 }
		}		
		echo "] }";
			
	}	
		
	
	
}//ending controller

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */