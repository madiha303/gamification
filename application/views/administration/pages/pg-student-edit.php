
<form id="studentFrm" name="studentFrm"  enctype="multipart/form-data" method="post" action="<?PHP echo base_url();?>administration/student/save">
<fieldset>
<legend><h2><?php echo $page_title; ?></h2></legend>
<table  cellpadding="0" cellspacing="0">
	<tr>
    	<td><?php echo validation_errors('<div style="color:red;">','</div>'); ?></td>
    </tr>
	<tr>
    	<td>
        	<p>
                <label><strong>First Name:</strong></label>                    
                <input size="60" name="student_fname" id="student_fname" type="text" value="<?PHP echo set_value('student_fname',$row->student_fname);?>" class="validate[required]" />
            <strong>        	*</strong></p>
        	<p>
        	  <label><strong>Last Name:</strong></label>
        	  <input size="60" name="student_lname" id="student_lname" type="text" value="<?PHP echo set_value('student_lname',$row->student_lname);?>" class="validate[required]" />
   	      <strong>*</strong></p>  
          
          <p>
        	  <label><strong>Email:</strong></label>
        	  <input size="60" name="student_email" id="student_email" type="text" value="<?PHP echo set_value('student_email',$row->student_email);?>" class="validate[required],custom[email]" style="text-transform:none;" />
          <strong>*</strong></p>  
             
             <p>
              <label><strong>Roll Number:</strong></label>                    
                <input size="60" name="student_rollno" id="student_rollno" type="text" value="<?PHP echo set_value('student_rollno',$row->student_rollno);?>" class="validate[required]" />
          <strong>*</strong>             </p>          
             
             <p>
              <label><strong>Department:</strong></label>                    
                <input size="60" name="student_dept" id="student_dept" type="text" value="<?PHP echo set_value('student_dept',$row->student_dept);?>" class="validate[required]" />
             <strong>*</strong>             </p>
             
              <p>
                <label><strong>Session:</strong></label>                    
               <select name="student_session_start" id="student_session_start" style="width:100px;">
                 <option value="2000" <?php echo set_select('student_session_start', '2000'); ?> <?PHP if($row->student_session_start == '2000'){echo 'selected';}?>>2000</option>
                 <option value="2001"  <?php echo set_select('student_session_start', '2001'); ?> <?PHP if($row->student_session_start == '2001'){echo 'selected';}?>>2001</option>
                 <option value="2002"  <?php echo set_select('student_session_start', '2002'); ?> <?PHP if($row->student_session_start == '2002'){echo 'selected';}?>>2002</option>
                 <option value="2003"  <?php echo set_select('student_session_start', '2003'); ?> <?PHP if($row->student_session_start == '2003'){echo 'selected';}?>>2003</option>
                 <option value="2004"  <?php echo set_select('student_session_start', '2004'); ?> <?PHP if($row->student_session_start == '2004'){echo 'selected';}?>>2004</option>
                 <option value="2005"  <?php echo set_select('student_session_start', '2005'); ?> <?PHP if($row->student_session_start == '2005'){echo 'selected';}?>>2005</option>
                 <option value="2006"  <?php echo set_select('student_session_start', '2006'); ?> <?PHP if($row->student_session_start == '2006'){echo 'selected';}?>>2006</option>
                 <option value="2007"  <?php echo set_select('student_session_start', '2007'); ?> <?PHP if($row->student_session_start == '2007'){echo 'selected';}?>>2007</option>
                 <option value="2008"  <?php echo set_select('student_session_start', '2008'); ?> <?PHP if($row->student_session_start == '2008'){echo 'selected';}?>>2008</option>
                 <option value="2009"  <?php echo set_select('student_session_start', '2009'); ?> <?PHP if($row->student_session_start == '2009'){echo 'selected';}?>>2009</option>
                 <option value="2010"  <?php echo set_select('student_session_start', '2010'); ?> <?PHP if($row->student_session_start == '2010'){echo 'selected';}?>>2010</option>
                 <option value="2011"  <?php echo set_select('student_session_start', '2011'); ?> <?PHP if($row->student_session_start == '2011'){echo 'selected';}?>>2011</option>
                 <option value="2012"  <?php echo set_select('student_session_start', '2012'); ?> <?PHP if($row->student_session_start == '2012'){echo 'selected';}?>>2012</option>
                 <option value="2013" selected="selected"  <?php echo set_select('student_session_start', '2013'); ?> <?PHP if($row->student_session_start == '2013'){echo 'selected';}?>>2013</option>
                 <option value="2014" <?php echo set_select('student_session_start', '2014'); ?> <?PHP if($row->student_session_start == '2014'){echo 'selected';}?>>2014</option>
                 <option value="2015" <?php echo set_select('student_session_start', '2015'); ?> <?PHP if($row->student_session_start == '2015'){echo 'selected';}?>>2015</option>
                 <option value="2016" <?php echo set_select('student_session_start', '2016'); ?> <?PHP if($row->student_session_start == '2016'){echo 'selected';}?>>2016</option>
                 <option value="2017" <?php echo set_select('student_session_start', '2017'); ?> <?PHP if($row->student_session_start == '2017'){echo 'selected';}?>>2017</option>
               </select>
               -
            <select name="student_session_end" id="student_session_end" style="width:100px;">
                 <option value="2000" <?php echo set_select('student_session_end', '2000'); ?> <?PHP if($row->student_session_end == '2000'){echo 'selected';}?>>2000</option>
                 <option value="2001"  <?php echo set_select('student_session_end', '2001'); ?> <?PHP if($row->student_session_end == '2001'){echo 'selected';}?>>2001</option>
                 <option value="2002"  <?php echo set_select('student_session_end', '2002'); ?> <?PHP if($row->student_session_end == '2002'){echo 'selected';}?>>2002</option>
                 <option value="2003"  <?php echo set_select('student_session_end', '2003'); ?> <?PHP if($row->student_session_end == '2003'){echo 'selected';}?>>2003</option>
                 <option value="2004"  <?php echo set_select('student_session_end', '2004'); ?> <?PHP if($row->student_session_end == '2004'){echo 'selected';}?>>2004</option>
                 <option value="2005"  <?php echo set_select('student_session_end', '2005'); ?> <?PHP if($row->student_session_end == '2005'){echo 'selected';}?>>2005</option>
                 <option value="2006"  <?php echo set_select('student_session_end', '2006'); ?> <?PHP if($row->student_session_end == '2006'){echo 'selected';}?>>2006</option>
                 <option value="2007"  <?php echo set_select('student_session_end', '2007'); ?> <?PHP if($row->student_session_end == '2007'){echo 'selected';}?>>2007</option>
                 <option value="2008"  <?php echo set_select('student_session_end', '2008'); ?> <?PHP if($row->student_session_end == '2008'){echo 'selected';}?>>2008</option>
                 <option value="2009"  <?php echo set_select('student_session_end', '2009'); ?> <?PHP if($row->student_session_end == '2009'){echo 'selected';}?>>2009</option>
                 <option value="2010"  <?php echo set_select('student_session_end', '2010'); ?> <?PHP if($row->student_session_end == '2010'){echo 'selected';}?>>2010</option>
                 <option value="2011"  <?php echo set_select('student_session_end', '2011'); ?> <?PHP if($row->student_session_end == '2011'){echo 'selected';}?>>2011</option>
                 <option value="2012"  <?php echo set_select('student_session_end', '2012'); ?> <?PHP if($row->student_session_end == '2012'){echo 'selected';}?>>2012</option>
                 <option value="2013"  <?php echo set_select('student_session_end', '2013'); ?> <?PHP if($row->student_session_end == '2013'){echo 'selected';}?>>2013</option>
                 <option value="2014" <?php echo set_select('student_session_end', '2014'); ?> <?PHP if($row->student_session_end == '2014'){echo 'selected';}?>>2014</option>
                 <option value="2015" <?php echo set_select('student_session_end', '2015'); ?> <?PHP if($row->student_session_end == '2015'){echo 'selected';}?>>2015</option>
                 <option value="2016" <?php echo set_select('student_session_end', '2016'); ?> <?PHP if($row->student_session_end == '2016'){echo 'selected';}?>>2016</option>
                 <option value="2017" selected="selected" <?php echo set_select('student_session_end', '2017'); ?> <?PHP if($row->student_session_end == '2017'){echo 'selected';}?>>2017</option>
               </select>
             </p>
             
              <p>
                <label><strong>Password:</strong></label>                    
                <input size="60" name="student_password" id="student_password" type="text" value="<?PHP echo set_value('student_password',$row->student_password);?>" class="validate[required,minSize[5]]" style="text-transform:none;"  />
             <strong>*</strong>              </p>
             
              <p>              	
                <label><strong>Photo (<?PHP echo $this->config->item('image_types');?>):</strong></label>
                <input size="48" name="photo" id="photo" type="file"/>
                <?PHP if($row->student_photo !=""){?>
      <p>
                <img src="<?PHP echo base_url();?>administration/student/get_image/<?PHP echo $row->student_photo;?>/313/217"/>
          </p>
				<?PHP }?>
              </p>
             

             
           
             
             <p>
             	<input type="submit" name="btnSubmit" id="btnSubmit" value="Save" /> 
                <input type="button" onclick="window.location='<?PHP echo base_url();?>administration/student'" value="cancel"/>               
             </p>
        </td>
    </tr>
</table>
<input type="hidden" name="mode" id="mode" value="<?PHP echo $mode;?>"/>
<input type="hidden" name="id" id="id" value="<?PHP if(isset($row)){echo $row->student_id;}?>"/>
</fieldset>
</form>

