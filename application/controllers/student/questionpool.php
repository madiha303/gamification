<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Questionpool extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 
	private $error = "";
	 
    public function __construct()
    {
		parent::__construct();
		
		if(!$this->session->userdata('loggedinstudent'))
		{
			$this->session->set_flashdata('response', '<div class="error-box">Please login...!</div>');
			redirect(base_url().'student/login', 'refresh');		
			exit;
		}	
		
		// Your own constructor code    	
	}	
	 
	public function index()
	{
          
            $data = array(
				'page_title' => "Question Pool Management",
				'page_view' => "student/pages/pg-questionpool-view"
				);
														
		$this->load->view('student/shared/master',$data);
	}
	
	
//************** Get Table *****		
	public function get_table()
	{
	
	
		 $date1=date('Y-m-d H:i:s');
		
		$query = "SELECT (SELECT section
		FROM vbl_course
		WHERE vbl_course.course_id = vbl_student_course.course_id) AS section, 
				
		(SELECT course_title
		FROM vbl_course
		WHERE vbl_course.course_id = vbl_student_course.course_id) AS course_title, vbl_questionpool. * , vbl_student_course. *
		FROM vbl_questionpool, vbl_student_course
		
		WHERE vbl_questionpool.questionpool_id NOT IN 
		(SELECT questionpool_id 
		FROM vbl_student_questionpool_submission
		WHERE vbl_student_questionpool_submission.student_id =".$this->session->userdata('loggedinstudent')->student_id.")
		AND vbl_student_course.student_id =".$this->session->userdata('loggedinstudent')->student_id."
		AND vbl_questionpool.close_date > '".$date1."'
		AND vbl_questionpool.open_date < '".$date1."'
		AND vbl_questionpool.status = 'published'
		AND vbl_student_course.course_id = vbl_questionpool.course_id";
		
				
		$res=$this->db_model->sql($query);			
														
        echo "{ \"aaData\": [";
		if ($res)
        {
			 $indx = 1;
			 foreach ($res as $row)
             {
				 
				 $indx_id = $row->questionpool_id;
				 
				 $options = "<a href='".base_url()."student/questionpool/submit/".$indx_id."'>View & Submit</a>";
	
				 
				 if ($indx != sizeof($res))
                 {
					
					 
					  echo '["'.$row->title.'","'.$row->course_title.'","'.$row->section.'","'.date("F j, Y, g:i a",strtotime($row->open_date)).'","'.date("F j, Y, g:i a",strtotime($row->close_date)).'","'.$options.'"],';
				 }
				 else
				{
					 echo '["'.$row->title.'","'.$row->course_title.'","'.$row->section.'","'.date("F j, Y, g:i a",strtotime($row->open_date)).'","'.date("F j, Y, g:i a",strtotime($row->close_date)).'","'.$options.'"]';
				 }
				 
				 $indx++;
			 }
		}
		
		echo "] }";
			
	}		
//************** submit *****	
	
	public function submit($id)
	{
		$data = array(
				'error' => $this->error,
				'page_title' => "Questionpool Management",
				'page_view' => "student/pages/pg-questionpool-edit",
				'mode' => "edit",				
				'row' => $this->db_model->select_two_tables_where('','questionpool',array('questionpool.questionpool_id' =>$id),'course','course_id')
				
				//'row' => $this->db_model->select_multiple_joins_where_groupby('','assignment',array('assignment.id' =>$id),'student_course','course_id','course','course_id','')						
				);
														
		$this->load->view('student/shared/master',$data);
	}
	//************** view_submitted_assignment_tableTable *****		
	public function view_submitted_questionpool_table()
	{
          
            $data = array(
				'page_title' => "Submitted Questions",
				'page_view' => "student/pages/pg-submitted-questionpool-view"
				);
														
		$this->load->view('student/shared/master',$data);
	}
	
	//************** View Submitted Assignment Table *****		
	public function view_table()
	{
	
	
		 $date1=date('Y-m-d H:i:s');
		
		$query = "SELECT (SELECT section
		FROM vbl_course
		WHERE vbl_course.course_id = vbl_student_course.course_id) AS section, 
		
		
		
				
		(SELECT course_title
		FROM vbl_course
		WHERE vbl_course.course_id = vbl_student_course.course_id) AS course_title, vbl_questionpool. * , vbl_student_course. * ,vbl_student_questionpool_submission. *
		FROM vbl_questionpool, vbl_student_course, vbl_student_questionpool_submission
		
		WHERE vbl_questionpool.questionpool_id =vbl_student_questionpool_submission.questionpool_id
		AND vbl_student_questionpool_submission.student_id =".$this->session->userdata('loggedinstudent')->student_id."
		AND vbl_student_course.student_id =".$this->session->userdata('loggedinstudent')->student_id."
		AND vbl_student_course.course_id = vbl_questionpool.course_id";
		
				
		$res=$this->db_model->sql($query);		

/*$res=$this->db_model->sql("SELECT *
FROM vbl_assignment
INNER JOIN vbl_student_course ON vbl_student_course.course_id = vbl_assignment.course_id
INNER JOIN vbl_course ON vbl_assignment.course_id = vbl_course.course_id
INNER JOIN vbl_student_assignment_submission ON vbl_assignment.assignment_id = vbl_student_assignment_submission.assignment_id
WHERE vbl_student_course.student_id =".$this->session->userdata('loggedinstudent')->student_id." AND vbl_assignment.accept_until_date > '".$date1."' AND vbl_assignment.open_date < '".$date1."' AND vbl_assignment.status='published' AND vbl_student_assignment_submission.is_submitted!=".'1');*/		
														
        echo "{ \"aaData\": [";
		if ($res)
        {
			 $indx = 1;
			 foreach ($res as $row)
             {
				 
				 $indx_id = $row->questionpool_id;
				 

				 
				
					 
				 
				 //$options = "<a href='".base_url()."student/assignment/submitted_assignment/".$indx_id."'>View & Submit</a>";
	
				  if ($indx != sizeof($res))
                 {
					if($row->is_approved==1)
					{
						$value_is_approvved='Yes';
					}
					else
					{
						$value_is_approvved='No';
					}
					 
					  echo '["'.$row->title.'","'.$row->course_title.'","'.$row->section.'","'.date("F j, Y, g:i a",strtotime($row->submission_time)).'","'.$row->question.'"],';
				 }
				 else
				{
					if($row->is_approved==1)
					{
						$value_is_approvved='Yes';
					}
					else
					{
						$value_is_approvved='No';
					}
					 
					 echo '["'.$row->title.'","'.$row->course_title.'","'.$row->section.'","'.date("F j, Y, g:i a",strtotime($row->submission_time)).'","'.$row->question.'"]';
				 }
				 
				 $indx++;
			 }
		}
		
		echo "] }";
			
	}		
//************** submit *****	
	
	public function submitted_assignment($id)
	{
		$query = "SEzLECT (SELECT course_title
		FROM vbl_course
		WHERE vbl_course.course_id = vbl_assignment.course_id) AS course_title , vbl_assignment.* , vbl_student_assignment_submission.*
		FROM vbl_assignment, vbl_student_assignment_submission
		
		WHERE vbl_student_assignment_submission.student_id =".$this->session->userdata('loggedinstudent')->student_id."
	
		AND vbl_student_assignment_submission.assignment_id = '".$id."'";
		
		$data = array(
				'error' => $this->error,
				'page_title' => "Assignment Management",
				'page_view' => "student/pages/pg-assignment-edit",
				'mode' => "edit",				
				'row' => $this->db_model->select_multiple_joins_where_groupby('','assignment',array('student_assignment_submission.assignment_id' =>$id),'course','course_id','student_assignment_submission','assignment_id','student_assignment_submission.assignment_id')
				//'row'=>$this->db_model->sql($query)
				//'row' => $this->db_model->select_multiple_joins_where_groupby('','assignment',array('assignment.id' =>$id),'student_course','course_id','course','course_id','')						
				);
														
		$this->load->view('student/shared/master',$data);
	}
	
//************** save *****		
/*	public function save()
	{
		if($this->input->post())
		{
			$this->load->library('form_validation');
				$this->form_validation->set_rules('attachment', 'Attachment', 'required');
				
			
			if ($this->form_validation->run() == FALSE)
			{
				$this->load_view();	
			}
			else
			{
				if($this->input->post('mode')=="edit")
				{
					$this->insert();
				}
			}
			
		}
		
	}
*///************** delete *****	
	/*public function del($id)
	{
		
		$res = $this->db_model->delete_row("assignment",array('id'=>$id));
		
		if($res)
		{
			$this->session->set_flashdata('response', '<div class="success-box">Selected record has been deleted.</div>');
			redirect(base_url().'student/assignment', 'refresh');
		}
		else
		{
			$this->session->set_flashdata('response', '<div class="error-box">Request can not be processed at the moment, please try again later.</div>');
			redirect(base_url().'student/assignment', 'refresh');
		}
	}*/
	
//************** Update *****	
/*	public function update()
	{
		$vals = $this->input->post();
		unset($vals['btnSubmit'],$vals['mode'],$vals['id']);	
		
		if($_FILES['attachment']['name'] != "")
		{
			$attachment = $this->upload();
			$vals['attachment'] = $attachment;
		}
			
		$vals['last_modified'] = date('Y-m-d h:i:s');
		$vals['open_date'] = date('Y-m-d H:i:s', strtotime($vals['open_date']));
		$vals['due_date'] = date('Y-m-d H:i:s', strtotime($vals['due_date']));
		$vals['accept_until_date'] = date('Y-m-d H:i:s', strtotime($vals['accept_until_date']));
		
		//var_dump($vals);				
		//exit;				
		$where = array('id' => $this->input->post('id'));
		
		$res = $this->db_model->update_row('assignment',$vals,$where);
		
		if($res)
		{
			$this->session->set_flashdata('response', '<div class="success-box">Information has been modified.</div>');
			redirect(base_url().'student/assignment/edit/'.$this->input->post('id').'', 'refresh');
		}
		else
		{
			$this->session->set_flashdata('response', '<div class="error-box">Request can not be processed at the moment, please try again later.</div>');
			redirect(base_url().'student/assignment/edit/'.$this->input->post('id').'', 'refresh');
		}
	}*/
//************** Insert *****		
	public function insert()
	{
		
		$vals = $this->input->post();
		$where = array('student_questionpool_submission.student_id' =>$this->session->userdata('loggedinstudent')->student_id,'student_questionpool_submission.questionpool_id' => $vals['questionpool_id']);
		
							
		$user = $this->db_model->get_row("student_questionpool_submission",$where);
		
		if($user)
		{
			$this->session->set_flashdata('response', '<div class="error-box">You have already submitted question.</div>');
			redirect(base_url().'student/questionpool', 'refresh');
			//var_dump($user);
		}
		else
		{
				unset($vals['btnSubmit'],$vals['mode'],$vals['id'],$vals['frmts']);
				
				/*if($_FILES['attachment']['name'] != "")
				{
					$attachment = $this->upload();
					$vals['attachment'] = $attachment;
				}*/
				
				$vals['submission_time'] = date('Y-m-d H:i:s');
				//$vals['open_date'] = date('Y-m-d H:i:s', strtotime($vals['open_date']));	
				
				
				$vals['close_date'] = date('Y-m-d H:i:s', strtotime($vals['close_date']));
				
				
				$final_day=(strtotime($vals['close_date'])-strtotime($vals['submission_time']))/86400;
				unset($vals['close_date']);
				$vals['student_id']=$this->session->userdata('loggedinstudent')->student_id;
				//var_dump($vals);
				
				//$date1=  $vals['due_date'];
				//$date2= $vals['open_date'];
				//$date3= $date1->diff($date2)->format("%d");
				
				
				/*echo "<br><br>";
				
				$total_days=(strtotime($vals['due_date']) - strtotime($vals['open_date']))/86400;
				echo $total_days;
				//$total_days=$total_days/2;
				echo "<br>diffreence b/w open and sue date ".$total_days;
				$left_days=(strtotime($vals['due_date']) - strtotime($vals['submission_time']))/86400;*/
				//$datetime1 = strtotime(strtotime($vals['due_date']) - strtotime($vals['open_date']))/86400;($vals['open_date']);
				//$datetime2 = strtotime($vals['due_date']);
				// == return sec in difference
				//$secs = $datetime2-$datetime1;
				//$days = $secs / 86400;
				/*echo "<br><br> days left ". $left_days."<br><br>  ";
				//$days=$days/2;
				//print_r($days);
					if(($left_days<=$total_days)&&($left_days>=0))
					{
						$vals['obtained_performance_marks'] =$vals['obtained_performance_marks'] ;
						$vals['early_submission']=1;
					}
					else
					{
						$vals['obtained_performance_marks'] =0;
						$vals['early_submission']=0;
					}
					echo "<br><br> performance marks ". $vals['obtained_performance_marks']."<br><br>  ";
					
				unset($vals['due_date'],$vals['open_date']);*/
				$vals['is_submitted']=1;	
				//var_dump($vals);
				//exit;
				if($final_day>=0)
				{
					$ret_id = $this->db_model->insert_row_retid("student_questionpool_submission",$vals);
				}
				//var_dump($ret_id);
				if($ret_id>0)
				{						
					$this->session->set_flashdata('response', '<div class="success-box">Your assignment has been submitted .</div>');
					redirect(base_url().'student/questionpool', 'refresh');
				}
				else
				{
					$this->session->set_flashdata('response', '<div class="error-box">Request can not be processed at the moment, please try again later.</div>');
					redirect(base_url().'student/questionpool', 'refresh');
				}
		}
		
		
	}

	
//**************Upload Path
	/*private function upload($field = 'attachment')
	{
		$path = './assets/submissions/';
		$config['upload_path'] = $path;
		$config['allowed_types'] = $this->config->item('files_types');
		$config['max_size']	= '3072';
		$config['remove_spaces'] = true;
		$config['encrypt_name'] = false;
		
		$this->load->library('upload', $config);
		$this->upload->initialize($config);

		if ( ! $this->upload->do_upload($field))
		{
			$this->error = $this->upload->display_errors('', '<br/>');
			$this->load_view();
			die($this->output->get_output());									
		}
		else
		{						
			$data = $this->upload->data();
			//$this->image_moo->load($path.$data['file_name']."")->resize("800","640")->save_pa($prepend="", $append="", $overwrite=FALSE);
			return $data['file_name'];
		}
	}*/
	
//************** Load View *****		
	private function load_view()
	{
		if($this->input->post('mode') == 'add')
		{
			$this->add();
			
		}
		else if($this->input->post('mode') == 'edit')
		{
			$this->edit($this->input->post('id'));		
		}
	}	
//************** initialize form *****		
	private function intialize_form()
	{
		$values = (object) array(
				 'id' => '',
				 'title' => '',
				 'detail' => '',
				 'access' => '',	
				 'status' => '',	
				 'begining_date'=>'',
				 'ending_date'=>'',
				 'attachment'=>'',	
				  'notification'=>''	 
				);
						
		return $values;
	}	
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */