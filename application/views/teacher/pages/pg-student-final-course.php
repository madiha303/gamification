<script>

$(function(){
	//$('#begining_date').datetimepicker({timeFormat: "hh:mm tt"});
	var startDateTextBox = $('#begining_date');
	var endDateTextBox = $('#ending_date');

	startDateTextBox.datetimepicker({ 
		onClose: function(dateText, inst) {
			if (endDateTextBox.val() != '') {
				var testStartDate = startDateTextBox.datetimepicker('getDate');
				var testEndDate = endDateTextBox.datetimepicker('getDate');
				if (testStartDate > testEndDate)
					endDateTextBox.datetimepicker('setDate', testStartDate);
			}
			else {
				endDateTextBox.val(dateText);
			}
		},
		onSelect: function (selectedDateTime){
			endDateTextBox.datetimepicker('option', 'minDate', startDateTextBox.datetimepicker('getDate') );
		}
	});
	endDateTextBox.datetimepicker({ 
		onClose: function(dateText, inst) {
			if (startDateTextBox.val() != '') {
				var testStartDate = startDateTextBox.datetimepicker('getDate');
				var testEndDate = endDateTextBox.datetimepicker('getDate');
				if (testStartDate > testEndDate)
					startDateTextBox.datetimepicker('setDate', testEndDate);
			}
			else {
				startDateTextBox.val(dateText);
			}
		},
		onSelect: function (selectedDateTime){
			startDateTextBox.datetimepicker('option', 'maxDate', endDateTextBox.datetimepicker('getDate') );
		}
	});

	
});



function countChars() {
     var l = "1000";
     var str = document.getElementById("detail").value;
     var len = str.length;
     if(len <= l) {
          document.getElementById("txtLen").value=l-len;
     } else {
          document.getElementById("detail").value=str.substr(0, 1000);
     }
}

			
</script>
<?php

if(isset($row))
{
	
	//var_dump($row);
	foreach($row as $rows){
	$value_id=$rows->student_course_id;
	$obtained_quiz=$rows->obtained_quiz;
	$obtained_final=$rows->obtained_final; 
	$obtained_mid=$rows->obtained_mid;
	$course_id=$rows->course_id;
	
	}
	
	
}


?>

<form id="announementFrm" name="announementFrm"  enctype="multipart/form-data" method="post" action="<?PHP echo base_url();?>teacher/submittedfinalgrade/save">
<fieldset>
<legend><h2><?php echo $page_title; ?></h2></legend>
<table  cellpadding="0" cellspacing="0">
	<tr>
    	<td><?php echo validation_errors('<div style="color:red;">','</div>'); ?></td>
    </tr>
	<tr>
    	<td>
        	
        
            <p>
                <label><strong>Quiz Marks:</strong></label>                    
                <input size="60" name="obtained_quiz" id="obtained_quiz" type="text" value="<?PHP if(isset($row)){echo set_value('lecture_title',$obtained_quiz);}?>" class="validate[required,custom[integer]] text-input" />
        	</p>
            
            <p>
                <label><strong>Mid Term Marks:</strong></label>                    
                <input size="60" name="obtained_mid" id="obtained_mid" type="text" value="<?PHP if(isset($row)){echo set_value('lecture_title',$obtained_mid);}?>" class="validate[required,custom[integer] text-input"  />
        	</p>
            
            <p>
                <label><strong>Final Term Marks:</strong></label>                    
                <input size="60" name="obtained_final" id="obtained_final" type="text" value="<?PHP if(isset($row)){echo set_value('lecture_title',$obtained_final);}?>" class="validate[required,custom[integer] text-input" />
        	</p>
            
           
             
             <p>
             	<input type="submit" name="btnSubmit" id="btnSubmit" value="Save" /> 
                <input type="button" onclick="window.location='<?PHP echo base_url();?>teacher/submittedfinalgrade/call_course_selected/<?=$course_id;?>'" value="cancel"/>               
             </p>
        </td>
    </tr>
</table>
<input type="hidden" name="mode" id="mode" value="<?PHP echo $mode;?>"/>
<input type="hidden" name="id" id="id" value="<?PHP if(isset($row)){echo $value_id;}?>"/>
<input type="hidden" name="last_modified" id="last_modified" value=""/>

</fieldset>
</form>

