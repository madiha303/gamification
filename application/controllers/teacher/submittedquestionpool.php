<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class submittedquestionpool extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 
	private $error = "";
	 
    public function __construct()
    {
		parent::__construct();
		
		if(!$this->session->userdata('loggedinteacher'))
		{
			$this->session->set_flashdata('response', '<div class="error-box">Please login...!</div>');
			redirect(base_url().'teacher/login', 'refresh');		
			exit;
		}	
		
		// Your own constructor code    	
	}	
	 
	public function index()
	{
          
            $data = array(
				'page_title' => "Mark Question Pool",
				'page_view' => "teacher/pages/pg-questionpool-view"
				);
														
		$this->load->view('teacher/shared/master',$data);
	}
	
//*************** Index 2******	
	public function index2()
	{
          
            $data = array(
				'page_title' => "Mark Question Pool",
				'page_view' => "teacher/pages/pg-all-submitted-questionpool-view"
				);
														
		$this->load->view('teacher/shared/master',$data);
	}
//************** Get Table *****		
	public function get_table()
	{
	
		// $res=$this->db_model->select_multiple_joins_where_groupby('assignment',array('teacher_course.teacher_id' =>$this->session->userdata('loggedinteacher')->teacher_id),'teacher_course','teacher_course_id','course','course_id','');
		 
		  $res=$this->db_model->select_multiple_joins_where_groupby(array('questionpool.*','course.*','questionpool.last_modified'),'questionpool',array('teacher_course.teacher_id' =>$this->session->userdata('loggedinteacher')->teacher_id),'teacher_course','teacher_id','course','course_id','questionpool_id');
														
        echo "{ \"aaData\": [";
		if ($res)
        {
			 $indx = 1;
			 foreach ($res as $row)
             {
				 
				 $indx_id = $row->questionpool_id;
				 
				// $view_url = "<a href='".base_url()."teacher/assignment/view/".$indx_id."'><img src='".base_url()."assets/images/teacher/icons/view_new_icon3.png'/></a>";
				
				 $edit_url = "<a href='".base_url()."teacher/questionpool/edit/".$indx_id."'><img src='".base_url()."assets/images/teacher/icons/edit.gif'/></a>";
				 
				 $del_url = "<a href='".base_url()."teacher/questionpool/del/".$indx_id."' onclick='return cnfrm()'><img src='".base_url()."assets/images/teacher/icons/del.gif'/></a>";	
					 
				 
				 $options = $edit_url." | ".$del_url;
	
				 
				 if ($indx != sizeof($res))
                 {
					  echo '["'.$row->title.'","'.$row->course_title.'","'.$row->section.'","'.date("F j, Y, g:i a",strtotime($row->open_date)).'","'.date("F j, Y, g:i a",strtotime($row->close_date)).'","'.date("F j, Y, g:i a",strtotime($row->last_modified)).'","'.$row->status.'","'.$options.'"],';
				 }
				 else
				{
					 echo '["'.$row->title.'","'.$row->course_title.'","'.$row->section.'","'.date("F j, Y, g:i a",strtotime($row->open_date)).'","'.date("F j, Y, g:i a",strtotime($row->close_date)).'","'.date("F j, Y, g:i a",strtotime($row->last_modified)).'","'.$row->status.'","'.$options.'"]';
				 }
				 
				 $indx++;
			 }
		}
		
		echo "] }";
			
	}		
//************** edit *****	
	
	public function edit($id)
	{
		$data = array(
				'error' => $this->error,
				'page_title' => "Assignment Management",
				'page_view' => "teacher/pages/pg-questionpool-edit",
				'mode' => "edit",				
				'row_course' => $this->db_model->select_two_tables_where('','teacher_course',array('teacher_id' =>$this->session->userdata('loggedinteacher')->teacher_id),'course','course_id'),
				'row' => $this->db_model->select_multiple_joins_where_groupby('','questionpool',array('questionpool.questionpool_id' =>$id),'teacher_course','teacher_id','course','course_id','questionpool_id')						
				);
														
		$this->load->view('teacher/shared/master',$data);
	}
	
	public function add()
	{				
		$data = array(
				'error' => $this->error,
				'page_title' => "Assignment Management",
				'page_view' => "teacher/pages/pg-questionpool-edit",
				'mode' => "add",
				'row_course' => $this->db_model->select_two_tables_where('','teacher_course',array('teacher_id' =>$this->session->userdata('loggedinteacher')->teacher_id),'course','course_id'),
				'form_row'=> $this->intialize_form()		
				);
														
		$this->load->view('teacher/shared/master',$data);
	}	
//************** save *****		
	public function save()
	{
		if($this->input->post())
		{
			$this->load->library('form_validation');
				$this->form_validation->set_rules('title', 'Title', 'required');
				$this->form_validation->set_rules('detail', 'Detail', 'required');
			
			if ($this->form_validation->run() == FALSE)
			{
				$this->load_view();	
			}
			else
			{
				if($this->input->post('mode')=="edit")
				{
					$this->update();
				}
				else if($this->input->post('mode')=="add")
				{
					$this->insert();
				}	
			}
			
		}
		else
		{
			$this->add();
		}
	}
//************** delete *****	
	public function del($id)
	{
		
		$res = $this->db_model->delete_row("questionpool",array('questionpool_id'=>$id));
		
		if($res)
		{
			$this->session->set_flashdata('response', '<div class="success-box">Selected record has been deleted.</div>');
			redirect(base_url().'teacher/questionpool', 'refresh');
		}
		else
		{
			$this->session->set_flashdata('response', '<div class="error-box">Request can not be processed at the moment, please try again later.</div>');
			redirect(base_url().'teacher/questionpool', 'refresh');
		}
	}
	
//************** Update *****	
	public function update()
	{
		$vals = $this->input->post();
		unset($vals['btnSubmit'],$vals['mode'],$vals['id']);	
		
		if($_FILES['attachment']['name'] != "")
		{
			$attachment = $this->upload();
			$vals['attachment'] = $attachment;
		}
		//var_dump($vals);
		//echo "<br><br><br>";	
		$vals['last_modified'] = date('Y-m-d H:i:s');
		$vals['open_date'] = date('Y-m-d H:i:s', strtotime($vals['open_date']));
		$vals['close_date'] = date('Y-m-d H:i:s', strtotime($vals['close_date']));
		//$vals['accept_until_date'] = date('Y-m-d H:i:s', strtotime($vals['accept_until_date']));
		//var_dump($vals);				
		//exit;				
		$where = array('questionpool_id' => $this->input->post('id'));
		
		$res = $this->db_model->update_row('questionpool',$vals,$where);
		
		if($res)
		{
			$this->session->set_flashdata('response', '<div class="success-box">Information has been modified.</div>');
			redirect(base_url().'teacher/questionpool/edit/'.$this->input->post('id').'', 'refresh');
		}
		else
		{
			$this->session->set_flashdata('response', '<div class="error-box">Request can not be processed at the moment, please try again later.</div>');
			redirect(base_url().'teacher/questionpool/edit/'.$this->input->post('id').'', 'refresh');
		}
	}
//************** Insert *****		
	public function insert()
	{
		
		$vals = $this->input->post();		
		unset($vals['btnSubmit'],$vals['mode'],$vals['id']);
		
		if($_FILES['attachment']['name'] != "")
		{
			$attachment = $this->upload();
			$vals['attachment'] = $attachment;
		}
		
		$vals['last_modified'] = date('Y-m-d h:i:s');
		$vals['open_date'] = date('Y-m-d H:i:s', strtotime($vals['open_date']));	
		$vals['close_date'] = date('Y-m-d H:i:s', strtotime($vals['close_date']));
		//$vals['accept_until_date'] = date('Y-m-d H:i:s', strtotime($vals['accept_until_date']));				
		
		$vals['teacher_id']=$this->session->userdata('loggedinteacher')->teacher_id;
		//var_dump($vals);
		//exit;
		$ret_id = $this->db_model->insert_row_retid("questionpool",$vals);
		//var_dump($ret_id);
		if($ret_id>0)
		{						
			$this->session->set_flashdata('response', '<div class="success-box">Information has been added.</div>');
			redirect(base_url().'teacher/questionpool', 'refresh');
		}
		else
		{
			$this->session->set_flashdata('response', '<div class="error-box">Request can not be processed at the moment, please try again later.</div>');
			redirect(base_url().'teacher/questionpool/add', 'refresh');
		}
	}
/*//************** Get image *****		
	public function get_image($img_name="",$width="",$height="")
	{
		if($img_name == "" || $width <=0 || $height <=0)
		{
			exit;
		}
		else
		{
			$img_name = str_replace("%20"," ",$img_name);
			$img = "./assets/students/".$img_name."";
			$this->image_moo->load($img)->resize($width,$height)->save_dynamic();
			if ($this->image_moo->errors) print $this->image_moo->display_errors(); 		
		}		
	}*/
	
//**************Upload Path
	private function upload($field = 'attachment')
	{
		$path = './assets/questionpool/';
		$config['upload_path'] = $path;
		$config['allowed_types'] = $this->config->item('files_types');
		$config['max_size']	= '3072';
		$config['remove_spaces'] = true;
		$config['encrypt_name'] = false;
		
		$this->load->library('upload', $config);
		$this->upload->initialize($config);

		if ( ! $this->upload->do_upload($field))
		{
			$this->error = $this->upload->display_errors('', '<br/>');
			$this->load_view();
			die($this->output->get_output());									
		}
		else
		{						
			$data = $this->upload->data();
			//$this->image_moo->load($path.$data['file_name']."")->resize("800","640")->save_pa($prepend="", $append="", $overwrite=FALSE);
			return $data['file_name'];
		}
	}
	
//************** Load View *****		
	private function load_view()
	{
		if($this->input->post('mode') == 'add')
		{
			$this->add();
			
		}
		else if($this->input->post('mode') == 'edit')
		{
			$this->edit($this->input->post('id'));		
		}
	}	
//************** initialize form *****		
	private function intialize_form()
	{
		$values = (object) array(
				 'id' => '',
				 'title' => '',
				 'detail' => '',
				 'access' => '',	
				 'status' => '',	
				 'begining_date'=>'',
				 'ending_date'=>'',
				 'attachment'=>'',	
				  'notification'=>''	 
				);
						
		return $values;
	}	

//*************************all_submitted_questionpool_get_table**********
	public function all_submitted_questionpool_get_table()
	{
		$res = $this->db_model->select_two_tables_distinct_where('questionpool',array('questionpool.teacher_id' =>$this->session->userdata('loggedinteacher')->teacher_id),'student_questionpool_submission','questionpool_id','questionpool.questionpool_id');	

		
        echo "{ \"aaData\": [";
		if ($res)
        {
			 $indx = 1;
			 foreach ($res as $row)
             {
				 
				 $indx_id = $row->questionpool_id;
				// $indx_title=$row->title;
				 //$indx_vals=array('id'=>$row->assignment_id,'title'=>$row->title);
				 
				 $edit_url = "<a href='".base_url()."teacher/submittedquestionpool/call_selected_questionpool/".$indx_id."'><img src='".base_url()."assets/images/teacher/icons/view_new_icon3.png'/></a>";
				
					 
				 
				 $options = $edit_url;
	
				 
				 if ($indx != sizeof($res))
                 {
					  echo '["'.$row->title.'","'.$row->detail.'","'.date("F j, Y, g:i a",strtotime($row->open_date)).'","'.date("F j, Y, g:i a",strtotime($row->close_date)).'","'.$options.'"],';
				 }
				 else
				{
					 echo '["'.$row->title.'","'.$row->detail.'","'.date("F j, Y, g:i a",strtotime($row->open_date)).'","'.date("F j, Y, g:i a",strtotime($row->close_date)).'","'.$options.'"]';
				 }
				 
				 $indx++;
			 }
		}
		
		echo "] }";
			
	}
	
	//************** call_selected_questionpool *****	
	
	public function call_selected_questionpool($id)
	{
	

		
		$data = array(
				'error' => $this->error,
				'page_title' => "Submitted Question Pool",
				'page_view' => "teacher/pages/pg-submitted-questionpool-view",
				'mode' => "edit",
				'id'=>$id							
				);
														
		$this->load->view('teacher/shared/master',$data);
	}
	
	
	
//************** View Table *****		
	public function view_table_selected_questionpool($id)
	{
		$where=array('questionpool_id'=>$id);
		
		//$res = $this->db_model->select_two_tables_distinct_where('assignment',array('assignment.teacher_id' =>$this->session->userdata('loggedinteacher')->teacher_id),'student_assignment_submission','assignment_id','assignment.assignment_id');	

		$res=$this->db_model->select_two_tables_where(array('student_questionpool_submission.*','student.*','student_questionpool_submission.last_modified'),'student_questionpool_submission',$where,'student','student_id');
		//get_rows('student_assignment_submission',$where);
        echo "{ \"aaData\": [";
		if ($res)
        {
			 $indx = 1;
			 foreach ($res as $row)
             {
				 
				 $indx_id = $row->student_questionpool_submission_id;
				// $indx_title=$row->title;
				 //$indx_vals=array('id'=>$row->assignment_id,'title'=>$row->title);
				 
				 //$grade_url = "<a href='".base_url()."teacher/gradebook/grade/".$indx_id."'>Grade / Edit</a>";
				
				$edit_url = "<a href='".base_url()."teacher/submittedquestionpool/approve_question/".$indx_id."'>Approve / Edit</a>";
				 
					 
				 
				 $options = $edit_url;//." | ".$grade_url;
	
				 
				 if ($indx != sizeof($res))
                 {
					 if($row->is_approved==0)
					 {
						 $value_is_approved='No';
					 }
					 else
					 {
						 $value_is_approved='Yes';
					 }
					 
					  echo '["'.$row->student_rollno.'","'.$value_is_approved.'","'.date("F j, Y, g:i a",strtotime($row->submission_time)).'","'.$row->total_marks.'","'.date("F j, Y, g:i a",strtotime($row->last_modified)).'","'.$options.'"],';
				 }
				 else
				{
					if($row->is_approved==0)
					 {
						 $value_is_approved='No';
					 }
					 else
					 {
						 $value_is_approved='Yes';
					 }
					
					 echo '["'.$row->student_rollno.'","'.$value_is_approved.'","'.date("F j, Y, g:i a",strtotime($row->submission_time)).'","'.$row->total_marks.'","'.date("F j, Y, g:i a",strtotime($row->last_modified)).'","'.$options.'"]';
				 }
				 
				 $indx++;
			 }
		}
		
		echo "] }";
			
	}	
	
	//************** grade *****	
	
	public function approve_question($id)
	{
		$where=array('student_questionpool_submission.student_questionpool_submission_id'=>$id);
		$data = array(
				'error' => $this->error,
				'page_title' => "Approve Question",
				'page_view' => "teacher/pages/pg-approve-questionpool-edit",
				'mode' => "edit",				
				'row' => $this->db_model->select_multiple_joins_where_groupby('student_questionpool_submission.*,questionpool.*,student.*','student_questionpool_submission',$where,'questionpool','questionpool_id','student','student_id','student_questionpool_submission_id')
				//'row' => $this->db_model->select_multiple_joins_where_groupby('','assignment',array('assignment.id' =>$id),'student_course','course_id','course','course_id','')						
				);
														
		$this->load->view('teacher/shared/master',$data);
	}
	

	//************** save *****		
	public function save_approved_question()
	{
		if($this->input->post())
		{
			$this->load->library('form_validation');
				$this->form_validation->set_rules('total_marks', 'total_marks', 'required');
			//$this->form_validation->set_rules('detail', 'Detail', 'required');
			
			if ($this->form_validation->run() == FALSE)
			{
				$this->load_view();	
			}
			else
			{
				if($this->input->post('mode')=="edit")
				{
					$this->update_approved_question();
				}
				/*else if($this->input->post('mode')=="add")
				{
					$this->update_approved_question();
				}	*/
			}
			
		}
		else
		{
			$this->add();
		}
	}
	
	//************** Update *****	
	public function update_approved_question()
	{
		$vals = $this->input->post();
		unset($vals['btnSubmit'],$vals['mode'],$vals['id']);	
		
		/*if($_FILES['attachment']['name'] != "")
		{
			$attachment = $this->upload();
			$vals['attachment'] = $attachment;
		}*/
			
		$vals['last_modified'] = date('Y-m-d H:i:s');
		//$vals['begining_date'] = date('Y-m-d H:i:s', strtotime($vals['begining_date']));
		//$vals['ending_date'] = date('Y-m-d H:i:s', strtotime($vals['ending_date']));
		
		//var_dump($vals);				
		//exit;				
		$where = array('student_questionpool_submission_id' => $vals['student_questionpool_submission_id']);
		//var_dump($where);
		//exit;
		$res = $this->db_model->update_row('student_questionpool_submission',$vals,$where);
		
		if($res)
		{
			$this->session->set_flashdata('response', '<div class="success-box">Information has been modified.</div>');
			redirect(base_url().'teacher/submittedquestionpool/call_selected_questionpool/'.$vals ["questionpool_id"].'', 'refresh');
		}
		else
		{
			$this->session->set_flashdata('response', '<div class="error-box">Request can not be processed at the moment, please try again later.</div>');
			redirect(base_url().'teacher/submittedquestionpool/call_selected_questionpool/'.$vals ["questionpool_id"].'', 'refresh');
		}
	}

	
	
}//ending contoller



/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */