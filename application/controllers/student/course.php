<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Course extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 
	private $error = "";
	 
    public function __construct()
    {
		parent::__construct();
		
		if(!$this->session->userdata('loggedinstudent'))
		{
			$this->session->set_flashdata('response', '<div class="error-box">Please login...!</div>');
			redirect(base_url().'student/login', 'refresh');		
			exit;
		}	
		
		// Your own constructor code    	
	}	
	 
	public function index()
	{
          
            $data = array(
				'page_title' => "Course Management",
				'page_view' => "student/pages/pg-course-view"
				);
														
		$this->load->view('student/shared/master',$data);
	}
public function get_table()
	{
		$res = $this->db_model->select_two_tables_where('','student_course',array('student_id' =>$this->session->userdata('loggedinstudent')->student_id),'course','course_id');
        echo "{ \"aaData\": [";
		if ($res)
        {
			 $indx = 1;
			 foreach ($res as $row)
             {
				 
				 $indx_id = $row->course_id;
				 
				 $edit_url = "<a href='".base_url()."student/course/call_course_progress/".$indx_id."'>View Progress</a>";
				 
				 $critaria_url = "<a href='".base_url()."teacher/course/call_criteria/".$indx_id."'>Weightage</a>";	
					 
				 
				 $options =$edit_url;
	
				 
				 if ($indx != sizeof($res))
                 {
					  echo '["'.$row->course_title.'","'.$row->course_code.'","'.$row->course_credit.'","'.$row->section.'","'.$row->assignment_weight.'","'.$row->question_weight.'","'.$row->invitation_weight.'","'.date("F j, Y, g:i a",strtotime($row->last_modified)).'","'.$options.'"],';
				 }
				 else
				{
					 echo '["'.$row->course_title.'","'.$row->course_code.'","'.$row->course_credit.'","'.$row->section.'","'.$row->assignment_weight.'","'.$row->question_weight.'","'.$row->invitation_weight.'","'.date("F j, Y, g:i a",strtotime($row->last_modified)).'","'.$options.'"]';
				 }
				 
				 $indx++;
			 }
		}
		
		echo "] }";
			
	}
//************
public function call_criteria($id)
	{
          
            $data = array(
				'page_title' => "Course Progress Criteria",
				'page_view' => "student/pages/pg-criteria-edit",
			
				 'mode' => "edit",
				'row' => $this->db_model->get_row('course',array('course_id' => $id))		
				);
														
		$this->load->view('student/shared/master',$data);
	}	
//************** save *****		
	public function save()
	{
		if($this->input->post())
		{
			$this->load->library('form_validation');
				$this->form_validation->set_rules('assignment_weight', 'assignment_weight', 'required');
				//$this->form_validation->set_rules('detail', 'Detail', 'required');
			
			if ($this->form_validation->run() == FALSE)
			{
				$this->load_view();	
			}
			else
			{
				if($this->input->post('mode')=="edit")
				{
					$this->update();
				}
				else if($this->input->post('mode')=="add")
				{
					$this->insert();
				}	
			}
			
		}
		else
		{
			$this->add();
		}
	}
//************** Update *****	
	public function update()
	{
		$vals = $this->input->post();
		unset($vals['btnSubmit'],$vals['mode'],$vals['id'],$vals['last_modified']);	
		
		/*if($_FILES['attachment']['name'] != "")
		{
			$attachment = $this->upload();
			$vals['attachment'] = $attachment;
		}*/
			
		$vals['last_modified'] = date('Y-m-d h:i:s');
		
		$vals['assignment_weight']=($vals['assignment_weight'])/100;
		$vals['invitation_weight']=($vals['invitation_weight'])/100;
		$vals['question_weight']=($vals['question_weight'])/100;
		$vals['quiz']=($vals['quiz'])/100;
		$vals['final_term']=($vals['final_term'])/100;
		$vals['mid_term']=($vals['mid_term'])/100;

		
		//var_dump($vals);				
		//exit;				
		$where = array('course_id' => $this->input->post('id'));
		
		$res = $this->db_model->update_row('course',$vals,$where);
		
		if($res)
		{
			$this->session->set_flashdata('response', '<div class="success-box">Information has been modified.</div>');
			redirect(base_url().'student/course/', 'refresh');
		}
		else
		{
			$this->session->set_flashdata('response', '<div class="error-box">Request can not be processed at the moment, please try again later.</div>');
			redirect(base_url().'student/course'.$this->input->post('id').'', 'refresh');
		}
	}	
		
//************
public function call_course_progress($id)
	{
          
            $data = array(
				'page_title' => "Course Progress Board",
				'page_view' => "student/pages/pg-course-progress-view",
				'id'=>$id
				);
														
		$this->load->view('student/shared/master',$data);
	}

//************** Get Table *****		
	public function show_progress($course_id)
	{
		
		 $sql = "select vbl_student.student_id,vbl_student.student_fname, vbl_student.student_lname,vbl_student.student_rollno from vbl_student
                 inner join (vbl_student_course) on
                 vbl_student.student_id = vbl_student_course.student_id and
                 vbl_student_course.course_id =" . $course_id . " ;";

        $res = $this->db_model->sql($sql);
		
		
        echo "{ \"aaData\": [";
		if ($res)
        {
			 $indx = 1;
			 foreach ($res as $row)
             {
				 
				 $indx_id = $row->student_id;
				 
				  $total_assignments_marks = $this->getAssignmentSubmissionMarks($course_id,$row->student_id);
				 
				  
				   $invitation_marks_arr = $this->getInvitationMarks($course_id, $row->student_id);
				   
				   $questionpool_marks = $this->getQuestionPoolMarks($course_id,$row->student_id);
				   
				
				 
				 	$weightage= $this->getWeightage($course_id);
					
				 $edit_url = "<a href='".base_url()."student/course/call_course_progress/".$indx_id."'>View Progress</a>";
				 
				 $del_url = "<a href='".base_url()."student/course/del/".$indx_id."' onclick='return cnfrm()'><img src='".base_url()."assets/images/student/icons/del.gif'/></a>";	
					 
				 
				 $options = $edit_url;
	
				 
				 if ($indx != sizeof($res))
                 {
					 $assignment_sum=$total_assignments_marks["total_obtained_assignment_marks"]+$total_assignments_marks["total_obtained_performance_marks"];
					 
					 $invitation_sum=$invitation_marks_arr["total_invitation_acceptance_marks"]+$invitation_marks_arr["total_invitation_attendance_marks"];
					 
					$total= (($assignment_sum * $weightage["assignment_weight"] )+ ($invitation_sum* $weightage["invitation_weight"] )+( $questionpool_marks * $weightage["question_weight"] ));
					 
					  echo '["'.$row->student_rollno.'","'.$row->student_fname.' '.$row->student_lname.'","'.$total_assignments_marks["total_obtained_performance_marks"].'","'.$total_assignments_marks["total_obtained_assignment_marks"].'","'.$assignment_sum.'","'.$invitation_marks_arr["total_invitation_acceptance_marks"].'","'.$invitation_marks_arr["total_invitation_attendance_marks"].'","'.$invitation_sum.'","'.$questionpool_marks.'","'.$total.'"],';
				 }
				 else
				{
					 $assignment_sum=$total_assignments_marks["total_obtained_assignment_marks"]+$total_assignments_marks["total_obtained_performance_marks"];
					 
					  $invitation_sum=$invitation_marks_arr["total_invitation_acceptance_marks"]+$invitation_marks_arr["total_invitation_attendance_marks"];
					 
					 	$total= (($assignment_sum * $weightage["assignment_weight"] )+ ($invitation_sum* $weightage["invitation_weight"] )+( $questionpool_marks * $weightage["question_weight"] ));
					 
					 echo '["'.$row->student_rollno.'","'.$row->student_fname.' '.$row->student_lname.'","'.$total_assignments_marks["total_obtained_performance_marks"].'","'.$total_assignments_marks["total_obtained_assignment_marks"].'","'.$assignment_sum.'","'.$invitation_marks_arr["total_invitation_acceptance_marks"].'","'.$invitation_marks_arr["total_invitation_attendance_marks"].'","'.$invitation_sum.'","'.$questionpool_marks.'","'.$total.'"]';
				 }
				 
				 $indx++;
			 }
		}
		
		echo "] }";
			
	}	
//*************************8
 function getWeightage($course_id)
    {
        $sql = "select  assignment_weight , invitation_weight , question_weight
		FROM vbl_course WHERE
                      vbl_course.course_id = ".$course_id. " ;";

        $result = $this->db_model->sql($sql);

        if ($result) {
            foreach ($result as $temp) {
              /*  $total_assignments_marks = $temp->total_assignments_marks;
                return $total_assignments_marks;*/
				
				 $assignment_weight = $temp->assignment_weight;
                $invitation_weight = $temp->invitation_weight;
				 $question_weight = $temp->question_weight;
				 

                $data["question_weight"] = $question_weight;

                $data["assignment_weight"] = $assignment_weight;
                $data["invitation_weight"] = $invitation_weight;
				
				/* $mid_term = $temp->mid_term;
                $final_term = $temp->final_term;
				 $quiz = $temp->quiz;
				 
				   $data["mid_term"] = $mid_term;

                $data["final_term"] = $final_term;
                $data["quiz"] = $quiz;
				 */
				 

                return $data;
				
				
            }
        }
    }
//**************************

 function getAssignmentSubmissionMarks($course_id, $student_id)
    {
        $sql = "select  COALESCE(sum(vbl_student_assignment_submission.obtained_assignment_marks),0) as obtained_assignment_marks,
  COALESCE(sum(vbl_student_assignment_submission.obtained_performance_marks),0) as obtained_performance_marks
                      from vbl_student_assignment_submission

                      inner join (vbl_student_course,vbl_assignment) on
                      vbl_student_course.student_id = ".$student_id. " and
                      vbl_student_assignment_submission.student_id = ".$student_id. " and
                      vbl_assignment.assignment_id  = vbl_student_assignment_submission.assignment_id and
                      vbl_assignment.course_id = ".$course_id. " and
                      vbl_student_course.course_id = ".$course_id. ";";

        $result = $this->db_model->sql($sql);

        if ($result) {
            foreach ($result as $temp) {
              /*  $total_assignments_marks = $temp->total_assignments_marks;
                return $total_assignments_marks;*/
				
				 $total_obtained_assignment_marks = $temp->obtained_assignment_marks;
                $total_obtained_performance_marks = $temp->obtained_performance_marks;

                $data["total_obtained_assignment_marks"] = $total_obtained_assignment_marks;
                $data["total_obtained_performance_marks"] = $total_obtained_performance_marks;

                return $data;
				
				
            }
        }
    }
//************************
    function getInvitationMarks($course_id, $student_id)
    {
        $sql = "select
 COALESCE(sum(vbl_student_invitation_acceptance.attendance_marks),0) as total_invitation_attendance_marks,
  COALESCE(sum(vbl_student_invitation_acceptance.acceptance_marks),0) as total_invitation_acceptance_marks

from vbl_student_invitation_acceptance

inner join (vbl_invitation) on
vbl_student_invitation_acceptance.student_id = " . $student_id . " and
vbl_student_invitation_acceptance.invitation_id = vbl_invitation.invitation_id and
vbl_invitation.course_id = " . $course_id . ";";

        $result = $this->db_model->sql($sql);

        if ($result) {
            foreach ($result as $temp) {
                $total_invitation_attendance_marks = $temp->total_invitation_attendance_marks;
                $total_invitation_acceptance_marks = $temp->total_invitation_acceptance_marks;

                $data["total_invitation_acceptance_marks"] = $total_invitation_acceptance_marks;
                $data["total_invitation_attendance_marks"] = $total_invitation_attendance_marks;

                return $data;
            }
        }
    }
//*********************************
 function getQuestionPoolMarks($course_id, $student_id)
    {
        $sql = "select
 COALESCE(sum(vbl_student_questionpool_submission.total_marks),0) as total_marks
from vbl_student_questionpool_submission

inner join (vbl_questionpool) on
vbl_student_questionpool_submission.student_id = ".$student_id. " and
vbl_student_questionpool_submission.questionpool_id = vbl_questionpool.questionpool_id and
vbl_questionpool.course_id =  ".$course_id. ";";

        $result = $this->db_model->sql($sql);

        if ($result) {
            foreach ($result as $temp) {
                $total_marks = $temp->total_marks;
                return $total_marks;
            }
        }
    }	

	
//*********************************
 function getstudent_course($course_id, $student_id)
    {
       $sql="SELECT * from vbl_student_course Where student_id=".$student_id. " and
course_id =  ".$course_id. ";";

        $result = $this->db_model->sql($sql);

        if ($result) {
            foreach ($result as $temp) {
                $total_marks = $temp->total_marks;
                return $total_marks;
            }
        }
    }	


//************** Load View *****		
	private function load_view()
	{
		if($this->input->post('mode') == 'add')
		{
			$this->add();
			
		}
		else if($this->input->post('mode') == 'edit')
		{
			$this->edit($this->input->post('id'));		
		}
	}	
//************** initialize form *****		
	private function intialize_form()
	{
		$values = (object) array(
				 'course_id' => '',
				 'course_title' => '',
				 'course_code' => '',
				 'course_credit' => '',			 
				);
						
		return $values;
	}	
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */