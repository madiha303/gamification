<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 
	public function index()
	{
		//$this->load->view('web/shared/master');
		   $data = array(
				'page_view' => "web/pages/pg-announcement-view"
				);
														
		$this->load->view('web/shared/master',$data);
	}
	
	public function toggle_vehicles()
	{		
		$data['vehicles'] = $this->db_model->get_rows('vehicles',array('v_type'=>''.$this->input->post('type').''));
		$this->load->view('web/pages/pg-vehicles',$data);
	}
	
	public function get_image($img_name="",$width="",$height="")
	{
		if($img_name == "" || $width <=0 || $height <=0)
		{
			exit;
		}
		else
		{
			$img_name = str_replace("%20"," ",$img_name);
			$img = "./assets/vehicles/".$img_name."";
			$this->image_moo->load($img)->resize($width,$height)->save_dynamic();
			if ($this->image_moo->errors) print $this->image_moo->display_errors(); 		
		}		
	}
	
	public function save()
	{
		if($this->input->post())
		{
			$this->load->library('form_validation');			
			$this->form_validation->set_message('required', '*');
			$this->form_validation->set_message('valid_email', '<br/>Please enter a valid email.');			
			$this->form_validation->set_rules('q_type', '', 'required');
			$this->form_validation->set_rules('manufacturer', '', 'required');
			$this->form_validation->set_rules('model', '', 'required');
			$this->form_validation->set_rules('colour', '', 'required');
			$this->form_validation->set_rules('contract_type', '', 'required');
			$this->form_validation->set_rules('payment_plan', '', 'required');
			$this->form_validation->set_rules('mileage', '', 'required');
			$this->form_validation->set_rules('maintenance', '', 'required');
			$this->form_validation->set_rules('name', '', 'required');
			$this->form_validation->set_rules('email', '', 'required|valid_email');
			$this->form_validation->set_rules('location', '', 'required');
			
			
			if ($this->form_validation->run() == FALSE)
			{
				$this->load->view('web/shared/master');
			}
			else
			{
				$this->send_email();
				$this->save_quote();
				$this->session->set_flashdata('response', '<div class="success">Thankyou for providing us the information, we will contact you very soon!</div>');
				redirect(base_url()."home/#quote",'refresh');	
			}
			
		}
		else
		{
			redirect(base_url(),'refresh');
		}
	}	
	
	private function send_email()
	{
		$admin_email = $this->db_model->get_row('users',array('id'=>5))->useremail;
		
		$message = "Dear Admin,<br/><br/>";
		
		$message .= "New request for quote has been received from vehiclebuyingandleasing.co.uk, Please find the details below:<br/><br/>";
				
		$message .="<strong>Personal Information</strong><br/><br/>";
		
		$message .= "<strong>Name:</strong> ".$this->input->post('name')."<br/>";
		$message .= "<strong>Email:</strong> ".$this->input->post('email')."<br/>";
		$message .= "<strong>Location:</strong> ".$this->input->post('location')."<br/><br/>";
		
		$message .="<strong>Vehicle Information</strong><br/><br/>";
		
		$message .= "<strong>Manufacturer:</strong> ".$this->input->post('manufacturer')."<br/>";
		$message .= "<strong>Model:</strong> ".$this->input->post('model')."<br/>";
		$message .= "<strong>Colour:</strong> ".$this->input->post('colour')."<br/>";
		$message .= "<strong>Mileage:</strong> ".$this->input->post('mileage')."<br/>";
		$message .= "<strong>Maintenance:</strong> ".$this->input->post('maintenance')."<br/><br/>";
		
		$message .="<strong>Contract Infromation</strong><br/><br/>";
		
		$message .= "<strong>Contract Type:</strong> ".$this->input->post('contract_type')."<br/>";
		$message .= "<strong>Payment Plan:</strong> ".$this->input->post('payment_plan')."<br/>";
		$message .= "<strong>Type:</strong> ".$this->input->post('q_type')."<br/><br/>";
		
		$message .= "<strong>Comments:</strong> ".$this->input->post('comments')."<br/>";
			
		$config['mailtype'] = 'html';				
		$this->email->initialize($config);
		$this->email->from($this->input->post('email'), $this->input->post('name'));
		$this->email->to($admin_email); 
		$this->email->subject('New request for quote.');
		$this->email->message($message);		
		
		$this->email->send();
		
	}
	
	private function save_quote()
	{	
		$vals = $this->input->post();
		unset($vals['x'],$vals['y']);
		$vals['submitted_on'] = date('Y-m-d h:i:s');
		$this->db_model->insert_row('quote',$vals);
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */