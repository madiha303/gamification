<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class CourseProgress extends CI_Controller
{


	

    function showProgress($course_id)
    {

        $sql = "select vbl_student.student_id,vbl_student.student_fname, vbl_student.student_lname,vbl_student.student_rollno from vbl_student
                 inner join (vbl_student_course) on
                 vbl_student.student_id = vbl_student_course.student_id and
                 vbl_student_course.course_id =" . $course_id . " ;";

        $result = $this->db_model->sql($sql);

        if ($result) {
            foreach ($result as $temp) {
                $student_id = $temp->student_id;
                $student_name = $temp->student_fname . " " . $temp->student_lname;
                $total_assignments_marks = $this->getAssignmentSubmissionMarks($course_id, $student_id);
                $invitation_marks_arr = $this->getInvitationMarks($course_id, $student_id);
                $questionpool_marks = $this->getQuestionPoolMarks($course_id, $student_id);
                echo $student_name . " , assignments marks: " . $total_assignments_marks["total_obtained_performance_marks"]. ", " . $total_assignments_marks["total_obtained_assignment_marks"];
                echo ",invitation marks " . $invitation_marks_arr["total_invitation_attendance_marks"] . ", " . $invitation_marks_arr["total_invitation_acceptance_marks"];
                echo ", question pool marks : " . $questionpool_marks;
            }
        }

    }


    function getInvitationMarks($course_id, $student_id)
    {
        $sql = "select
 COALESCE(sum(vbl_student_invitation_acceptance.attendance_marks),0) as total_invitation_attendance_marks,
  COALESCE(sum(vbl_student_invitation_acceptance.acceptance_marks),0) as total_invitation_acceptance_marks

from vbl_student_invitation_acceptance

inner join (vbl_invitation) on
vbl_student_invitation_acceptance.student_id = " . $student_id . " and
vbl_student_invitation_acceptance.invitation_id = vbl_invitation.invitation_id and
vbl_invitation.course_id = " . $course_id . ";";

        $result = $this->db_model->sql($sql);

        if ($result) {
            foreach ($result as $temp) {
                $total_invitation_attendance_marks = $temp->total_invitation_attendance_marks;
                $total_invitation_acceptance_marks = $temp->total_invitation_acceptance_marks;

                $data["total_invitation_acceptance_marks"] = $total_invitation_acceptance_marks;
                $data["total_invitation_attendance_marks"] = $total_invitation_attendance_marks;

                return $data;
            }
        }
    }

    function getAssignmentSubmissionMarks($course_id, $student_id)
    {
        $sql = "select  COALESCE(sum(vbl_student_assignment_submission.obtained_assignment_marks),0) as obtained_assignment_marks,
  COALESCE(sum(vbl_student_assignment_submission.obtained_performance_marks),0) as obtained_performance_marks
                      from vbl_student_assignment_submission

                      inner join (vbl_student_course,vbl_assignment) on
                      vbl_student_course.student_id = ".$student_id. " and
                      vbl_student_assignment_submission.student_id = ".$student_id. " and
                      vbl_assignment.assignment_id  = vbl_student_assignment_submission.assignment_id and
                      vbl_assignment.course_id = ".$course_id. " and
                      vbl_student_course.course_id = ".$course_id. ";";

        $result = $this->db_model->sql($sql);

        if ($result) {
            foreach ($result as $temp) {
              /*  $total_assignments_marks = $temp->total_assignments_marks;
                return $total_assignments_marks;*/
				
				 $total_obtained_assignment_marks = $temp->obtained_assignment_marks;
                $total_obtained_performance_marks = $temp->obtained_performance_marks;

                $data["total_obtained_assignment_marks"] = $total_obtained_assignment_marks;
                $data["total_obtained_performance_marks"] = $total_obtained_performance_marks;

                return $data;
				
				
            }
        }
    }

    function getQuestionPoolMarks($course_id, $student_id)
    {
        $sql = "select
 COALESCE(sum(vbl_student_questionpool_submission.total_marks),0) as total_marks
from vbl_student_questionpool_submission

inner join (vbl_questionpool) on
vbl_student_questionpool_submission.student_id = ".$student_id. " and
vbl_student_questionpool_submission.questionpool_id = vbl_questionpool.questionpool_id and
vbl_questionpool.course_id =  ".$course_id. ";";

        $result = $this->db_model->sql($sql);

        if ($result) {
            foreach ($result as $temp) {
                $total_marks = $temp->total_marks;
                return $total_marks;
            }
        }
    }
}