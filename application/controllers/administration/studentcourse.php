<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Studentcourse extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 
	private $error = "";
	 
    public function __construct()
    {
		parent::__construct();
		
		if(!$this->session->userdata('loggedinuser'))
		{
			$this->session->set_flashdata('response', '<div class="error-box">Please login...!</div>');
			redirect(base_url().'administration/login', 'refresh');		
			exit;
		}	
		
		// Your own constructor code    	
	}	
	 
	public function index()
	{
          
            $data = array(
				'page_title' => "Course Assignment to Student ",
				'page_view' => "administration/pages/pg-student-course-view"
				);
														
		$this->load->view('administration/shared/master',$data);
	}
	
	
	public function index2()
	{
          
            $data = array(
				'page_title' => "Assign Course ",
				'page_view' => "administration/pages/pg-assign-cpanel"
				);
														
		$this->load->view('administration/shared/master',$data);
	}
//************** Get Table *****		
	public function get_table()
	{
		$res = $this->db_model->select_multiple_tables(array('student_course.student_course_id','course.*','student.*','student_course.last_modified'),'student_course','course','course_id','student','student_id');
       // var_dump($res);
		echo "{ \"aaData\": [";
		if ($res)
		
        {
			 $indx = 1;
			 foreach ($res as $row)
             {
				 
				 $indx_id = $row->student_course_id;
				 
				 $edit_url = "<a href='".base_url()."administration/studentcourse/edit/".$indx_id."'><img src='".base_url()."assets/images/administration/icons/edit.gif'/></a>";
				 
				 $del_url = "<a href='".base_url()."administration/studentcourse/del/".$indx_id."' onclick='return cnfrm()'><img src='".base_url()."assets/images/administration/icons/del.gif'/></a>";	
					 
				 
				 $options = $edit_url." | ".$del_url;
				 
				 $photo = "-";
				 
				 if ($indx != sizeof($res))
                 {
					 
					  echo '["'.$row->course_title.'","'.$row->section.'","'.$row->student_rollno.'","'.$row->student_fname.' '.$row->student_lname.'","'.$row->student_dept.'","'.$row->course_code.'","'.date("F j, Y, g:i a",strtotime($row->last_modified)).'","'.$options.'"],';
				 }
				 else
				 {
					 	
					 echo '["'.$row->course_title.'","'.$row->section.'","'.$row->student_rollno.'","'.$row->student_fname.' '.$row->student_lname.'","'.$row->student_dept.'","'.$row->course_code.'","'.date("F j, Y, g:i a",strtotime($row->last_modified)).'","'.$options.'"]';
				 }
				 
				 $indx++;
			 }
		}
		
		echo "] }";
			
	}	
//************** edit *****	
	
	public function edit($id)
	{
		$data = array(
				'error' => $this->error,
				'page_title' => "Course Assignment to Student",
				'page_view' => "administration/pages/pg-student-course-edit",
				'mode' => "edit",
				'row_course' => $this->db_model->get_table('course'),
				'row_student' => $this->db_model->get_table('student'),
				'row' => $this->db_model->select_multiple_tables_where('student_course',array('student_course_id' => $id),'course','course_id','student','student_id')				
				);
														
		$this->load->view('administration/shared/master',$data);
	}
	
	public function add()
	{				
		$data = array(
				'error' => $this->error,
				'page_title' => "Course Assignment to Student",
				'page_view' => "administration/pages/pg-student-course-edit",
				'mode' => "add",
				'row_course' => $this->db_model->get_table('course'),
				'row_student' => $this->db_model->get_table('student'),
				'form_row'=> $this->intialize_form()
						
				);
														
		$this->load->view('administration/shared/master',$data);
	}	
//************** save *****		
	public function save()
	{
		if($this->input->post())
		{
			$this->load->library('form_validation');
			//$this->form_validation->set_rules('teacher_name', 'Teacher Name', 'required');
			//$this->form_validation->set_rules('course_name', 'Course Name', 'required');
			
			/*if ($this->form_validation->run() == FALSE)
			{
				$this->load_view();	
			}
			else
			{*/
				if($this->input->post('mode')=="edit")
				{
					$this->update();
				}
				else if($this->input->post('mode')=="add")
				{
					$this->insert();
				}	
		//	}
			
		}
		else
		{
			$this->add();
		}
	}
//************** delete *****	
	public function del($id)
	{
		
		$res = $this->db_model->delete_row("student_course",array('student_course_id'=>$id));
		
		if($res)
		{
			$this->session->set_flashdata('response', '<div class="success-box">Selected record has been deleted.</div>');
			redirect(base_url().'administration/studentcourse', 'refresh');
		}
		else
		{
			$this->session->set_flashdata('response', '<div class="error-box">Request can not be processed at the moment, please try again later.</div>');
			redirect(base_url().'administration/studentcourse', 'refresh');
		}
	}
	
//************** Update *****	
	public function update()
	{
		$vals = $this->input->post();
		unset($vals['btnSubmit'],$vals['mode'],$vals['id']);	
		
		
			
		$vals['last_modified'] = date('Y-m-d h:i:s');
				//var_dump($vals);
				//exit;		
		$where = array('student_course_id' => $this->input->post('id'));
		
		$res = $this->db_model->update_row('student_course',$vals,$where);
		
		if($res)
		{
			$this->session->set_flashdata('response', '<div class="success-box">Information has been modified.</div>');
			redirect(base_url().'administration/studentcourse/edit/'.$this->input->post('id').'', 'refresh');
		}
		else
		{
			$this->session->set_flashdata('response', '<div class="error-box">Request can not be processed at the moment, please try again later.</div>');
			redirect(base_url().'administration/studentcourse/edit/'.$this->input->post('id').'', 'refresh');
		}
	}
//************** Insert *****		
	public function insert()
	{
		
		$vals = $this->input->post();		
		unset($vals['btnSubmit'],$vals['mode'],$vals['id']);
		
		
		$vals['last_modified'] = date('Y-m-d h:i:s');				
		var_dump($vals);
		//exit;
		$ret_id = $this->db_model->insert_row_retid("student_course",$vals);
		
		if($ret_id>0)
		{						
			$this->session->set_flashdata('response', '<div class="success-box">Information has been added.</div>');
			redirect(base_url().'administration/studentcourse', 'refresh');
		}
		else
		{
			$this->session->set_flashdata('response', '<div class="error-box">Request can not be processed at the moment, please try again later.</div>');
			redirect(base_url().'administration/studentcourse/add', 'refresh');
		}
	}


//************** Load View *****		
	private function load_view()
	{
		if($this->input->post('mode') == 'add')
		{
			$this->add();
			
		}
		else if($this->input->post('mode') == 'edit')
		{
			$this->edit($this->input->post('id'));		
		}
	}	
//************** initialize form *****		
	private function intialize_form()
	{
		$values = (object) array(
				 'id' => '',
				 'course_id' => '',
				 'teacher_id' => '',
									 
				);
						
		return $values;
	}	
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */