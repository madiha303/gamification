
<?php
/*$options = array('Computer Science'=>'Computer Science','Software Engineering'=>'Software Engineering', 'Computer Arts'=>'Computer Arts');
var_dump($row_teacher);
echo "<br/><br/><br/>";
var_dump($row_course);
echo "<br/><br/>";
var_dump($row);
echo "<br/><br/>";*/
if(isset($row))
{
	foreach($row as $val){
	$id=$val->student_course_id;
	$value_student=$val->student_fname.' '.$val->student_lname.'--->'.$val->student_dept;
	$value_course=$val->course_title.'   ('.$val->section.')';
	}
}

//var_dump($row_student);
//echo "<br/><br/>";
//var_dump($row_course);
?>
<form id="studentCourseFrm" name="studentCourseFrm"  enctype="multipart/form-data" method="post" action="<?PHP echo base_url();?>administration/studentcourse/save">
<fieldset>
<legend><h2><?php echo $page_title; ?></h2></legend>
<table  cellpadding="0" cellspacing="0">
	<tr>
    	<td><?php echo validation_errors('<div style="color:red;">','</div>'); ?></td>
    </tr>
	<tr>
    	<td>
        	<p>
                <label><strong>Student Name:</strong></label>                    
                <select name="student_id" id="student_id" style="width:382px;" class="dropdown validate[required]" >    
                 <option value="" <?php echo set_select('student_id', '', TRUE); ?>>Select Student</option>      
						<?php foreach($row_student as $rows): 
						 $key1=$rows->student_id;	
                         $value=$rows->student_fname.' '.$rows->student_lname.'--->'.$rows->student_dept; 
						 
						
						 ?>                                              
                        <option value="<?php echo $key1?>" 
                              <?php   
							  	if(isset($row))
								{
									if ($value == $value_student)
									{
										echo 'selected';
									}; 
								}
							  ?>
                          >
                                <?php  echo $value ?>
                        </option>
                       
                        <?php endforeach; ?>
            	</select>
        	</p>
        	<p>
        	  <label><strong>Course:</strong></label>
        	  <select name="course_id" id="course_id" style="width:382px;"  class="dropdown validate[required]">      
						 <option value="" <?php echo set_select('course_id', '', TRUE); ?>>Select Course</option>
						<?php foreach($row_course as $rows): 
						 $key1=$rows->course_id;
						 
						 	
                         $value=$rows->course_title.'   ('.$rows->section.')'; 
						
						 ?>                                              
                       
                        <option value="<?php echo $key1?>" 
                              <?php  
							   if(isset($row))
								{
								  	if ($value==$value_course)
									{
										echo 'selected';
									}; 
								}
								?>
                          >
                                <?php  echo $value ?>
                        </option>
                       
                        <?php endforeach; ?>
                        
            	</select>                
       	  </p>
          
      
				
            
             	<input type="submit" name="btnSubmit" id="btnSubmit" value="Save" /> 
                <input type="button" onclick="window.location='<?PHP echo base_url();?>administration/studentcourse'" value="cancel"/>               
             </p>
        </td>
    </tr>
</table>

<input type="hidden" name="mode" id="mode" value="<?PHP echo $mode;?>"/>
<input type="hidden" name="id" id="id" value="<?PHP if(isset($row)){echo $id;}?>"/>


</fieldset>
</form>

